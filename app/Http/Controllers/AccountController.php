<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use cookie;
use DB;
class AccountController extends Controller

{

    public function login()
    {
     return View('site.login');
    }

    public function doLogin(Request $request)
    {
      $response = [];
      $response['status'] = 'error';
      $params = array();
      $name = $request->nama;
      $password = $request->noKK;


      // dd($name, $password);
      $resultName = DB::table('kepala_keluarga')->where(['KepalaKeluarga'=>$name])->get();
      if ($resultName->isEmpty()) {
       $getAnggota = DB::table('anggota_keluarga')->where(['NamaLengkap'=>$name])->get();
       $idKeluarga = '';
       $NameAnggota = '';
       $userId = '';
       $noKK = '';
       if (!$getAnggota->isEmpty()) {
        foreach ($getAnggota as $row) {
         $userId = $row->Id;
         $idKeluarga = $row->IdKepalaKeluarga;
         $NameAnggota = $row->NamaLengkap;
        }
       }
       $getKK = DB::table('kepala_keluarga')->where(['Id'=>$idKeluarga])->get();
       if (!$getKK->isEmpty()) {
        foreach ($getKK as $row) {
         $noKK = $row->NomorKartu;
         }
       }
       if($noKK == $password) {
         $params = [
          'UserId' =>$userId,
          'Id' =>$idKeluarga,
          'KepalaKeluarga' =>$NameAnggota,
          'NoKK' => $noKK,
          'Role' => 'user'
         ];
          session::put('user', $params);
          $response['status'] = 'success';
          $response['message'] = 'Success login';
          $response['url'] = '/home';
          return response($response);
       }else{
        $response['message'] = 'Failed login';
        $response['url'] = '/login';
        return response($response);
       }
      }else{
       $result = DB::table('kepala_keluarga')->where(['KepalaKeluarga'=>$name,'NomorKartu'=>$password])->get();
       $params = [];
       $userId = '';
       foreach($result as $row){
        $params = [
        'UserId' =>$row->Id,
        'KepalaKeluarga' => $row->KepalaKeluarga,
        'NomorKartu' => $row->NomorKartu,
        'Alamat' => $row->Alamat,
        'Role' => $row->Role
       ];
       }
       if (!$result->isEmpty()) {
        session::put('user',$params);
        $response['status'] = 'success';
        $response['message'] = 'Success login';
        $response['url'] = '/home';
       } else {
        $response['message'] = 'Failed login';
        $response['url'] = '/login';
       }
       return response($response);
      }

    }

    public function logout(Request $request)
    {

      $request->session()->forget('user');
      return redirect(url('/login'));
    }

    public function listUser()
    {
     $result = DB::table('kepala_keluarga')->whereNotIn('Role', function($q){
               $q->select('Role')->where('Role', 'admin');
               })->get();
     // dd($result);
     return view('site.list', compact('result'));
    }

    public function register()
    {
     return view('site.register');
    }

    public function doRegHead(Request $request)
    {
     $params = [];
     $response = [];
     $response['status'] = 'error';
     $kk_numb = $request->NomorKartu;
     $nama_kepala_keluarga = $request->KepalaKeluarga;
     $alamat = $request->Alamat;

     $params = [
      'NomorKartu' => $kk_numb,
      'KepalaKeluarga' => $nama_kepala_keluarga,
      'Alamat' => $alamat,
      'Role' => 'user'
     ];

     $check = DB::table('kepala_keluarga')->where('NomorKartu', $params['NomorKartu'])->get();

     if(count($check) == 0 ){
      $result = DB::table('kepala_keluarga')->insertGetId($params);
      $response['status'] = 'succses';
      $response['id'] = $result;
     } else {
      $response['message'] = 'Failed Register';
     }
     //
     return response($response);
    }

    public function registerKeluarga(Request $request, $id)
    {

     return view('site.famRegis', compact('id'));

    }

    public function doRegFam(Request $request)
    {
     $params = [];
     $response = [];
     $response['status'] = 'error';
     $name = $request->NamaLengkap;
     $nik = $request->nik;
     $id = $request->id;

     // return $id;
     $params = [
      'NamaLengkap' => $name,
      'nik' => $nik,
      'IdKepalaKeluarga' => $id
     ];

     $check = DB::table('anggota_keluarga')->where('NIK', $params['nik'])->get();
     if(count($check) == 0 ){
      $result = DB::table('anggota_keluarga')->insert($params);
      $response['status'] = 'succses';
      $response['id'] = $result;
     } else {
      $response['message'] = 'Failed Register';
     }

     return response($response);
    }
}
