<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class LaporanController extends Controller
{
    public function index()
    {
     $resultKtp = DB::table('FormKtp')->get();
     $resultKK = DB::table('FormKK')->get();
     $resultNikah = DB::table('FormNikah')->get();
     $resultKematian = DB::table('FormKematian')->get();
     $resultKelahiran = DB::table('FormLahir')->get();

     return view('laporan.laporan', compact('resultKtp', 'resultKK', 'resultNikah','resultKematian', 'resultKelahiran'));
    }
}
