<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;

class KritikController extends Controller
{
    public function index()
    {
     $session = session('user');
     return view('kritik/kritik', compact('session'));
    }

    public function postKritik(Request $request)
    {
     $response = [];
     $response['status'] = 'error';
     $params = [];

     $params = [
      'NamaUser' => $request->name,
      'IsiPesan' => $request->kritik,
      'UserId' => $request->UserId
     ];
     $insert = DB::table('Kritik')->insert($params);

     if ($insert) {
      $response['status'] = 'success';
      $response['message'] = 'Success Insert';
      $response['url'] = '/home';
     } else {
      $response['message'] = 'Failed Inserts';
      $response['url'] = '/kritik';
     }
     return response($response);
    }
}
