<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use PDF;
use App;

class ItemController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function pdfktp(Request $request)
    {
     $formId = $request->id;
     $items = DB::table("FormKtp")->where('Id', $formId)->get();
     $pdf = PDF::loadView('pdf.pdfktp', compact('items'));
     $pdf->save(storage_path().'_filename.pdf');
     return $pdf->download('ticket_ktp.pdf');
    }

    public function pdfkk(Request $request)
    {
     $formId = $request->id;
     $items = DB::table("FormKK")->where('Id', $formId)->get();
     $pdf = PDF::loadView('pdf.pdfkk', compact('items'));
     $pdf->save(storage_path().'_filename.pdf');
     return $pdf->download('ticket_kk.pdf');
    }

    public function pdfnikah(Request $request)
    {
     $formId = $request->id;
     $items = DB::table("FormNikah")->where('Id', $formId)->get();
     $pdf = PDF::loadView('pdf.pdfnikah', compact('items'));
     $pdf->save(storage_path().'_filename.pdf');
     return $pdf->download('ticket_nikah.pdf');
    }

    public function pdflahir(Request $request)
    {
     $formId = $request->id;
     $items = DB::table("FormLahir")->where('Id', $formId)->get();
     $pdf = PDF::loadView('pdf.pdflahir', compact('items'));
     $pdf->save(storage_path().'_filename.pdf');
     return $pdf->download('ticket_form_kelahiran.pdf');
    }

    public function pdfkematian(Request $request)
    {
     $formId = $request->id;
     $items = DB::table("FormKematian")->where('Id', $formId)->get();
     $pdf = PDF::loadView('pdf.pdfkematian', compact('items'));
     $pdf->save(storage_path().'_filename.pdf');
     return $pdf->download('ticket_form_kematian.pdf');
    }

    public function printReport(Request $request)
    {
     $form = $request->type;
     $items = DB::table($form)->get();

     if ($form == 'FormKtp') {
      $pdf = PDF::loadView('pdf.report.reportKtp', compact('items'));
     }else if($form == 'FormKK'){
      $pdf = PDF::loadView('pdf.report.reportKk', compact('items'));
     }else if($form == 'FormNikah'){
      $pdf = PDF::loadView('pdf.report.reportNikah', compact('items'));
     }else if($form == 'FormLahir'){
      $pdf = PDF::loadView('pdf.report.reportLahir', compact('items'));
     }else if($form == 'FormKematian'){
      $pdf = PDF::loadView('pdf.report.reportKematian', compact('items'));
     }

     $pdf->save(storage_path().'_filename.pdf');
     if ($form == 'FormKtp') {
      return $pdf->download('report_form_ktp.pdf');
     }else if($form == 'FormKK'){
      return $pdf->download('report_form_kk.pdf');
     }else if($form == 'FormNikah'){
      return $pdf->download('report_form_nikah.pdf');
     }else if($form == 'FormLahir'){
      return $pdf->download('report_form_kelahiran.pdf');
     }else if($form == 'FormKematian'){
      return $pdf->download('report_form_kematian.pdf');
     }

    }
}
