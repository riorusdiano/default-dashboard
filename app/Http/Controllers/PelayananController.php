<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;

class PelayananController extends Controller
{
    public function index()
    {
     $session = session('user');
     return view('pelayanan/pelayanan', compact('session'));
    }

    public function ktp()
    {
     $session = session('user');
     return view('pelayanan.ktp', compact('session'));
    }

    public function listKtp()
    {
     $session = session('user');
     $result = DB::table('FormKtp')->get();
     return view('pelayanan.listKtp', compact('session', 'result'));
    }

    public function listNikah()
    {
     $session = session('user');
     $result = DB::table('FormNikah')->get();
     return view('pelayanan.listNikah', compact('session', 'result'));
    }

    public function listKelahiran()
    {
     $session = session('user');
     $result = DB::table('FormLahir')->get();
     return view('pelayanan.listKelahiran', compact('session', 'result'));
    }

    public function listKematian()
    {
     $session = session('user');
     $result = DB::table('FormKematian')->get();
     return view('pelayanan.listKematian', compact('session', 'result'));
    }

    public function myListKtp()
    {
     $session = session('user');
     $result = DB::table('FormKtp')->where('UserId', $session['UserId'])->get();
     return view('pelayanan.myListKtp', compact('session', 'result'));
    }

    public function myListNikah()
    {
     $session = session('user');
     $result = DB::table('FormNikah')->where('UserId', $session['UserId'])->get();
     return view('pelayanan.myListNikah', compact('session', 'result'));
    }

    public function myListLahir()
    {
     $session = session('user');
     $result = DB::table('FormLahir')->where('UserId', $session['UserId'])->get();
     return view('pelayanan.myListLahir', compact('session', 'result'));
    }

    public function myListKematian()
    {
     $session = session('user');
     $result = DB::table('FormKematian')->where('UserId', $session['UserId'])->get();
     return view('pelayanan.myListKematian', compact('session', 'result'));
    }

    public function listKk()
    {
     $session = session('user');
     $result = DB::table('FormKK')->get();
     return view('pelayanan.listKK', compact('session', 'result'));
    }

    public function myListKK()
    {
     $session = session('user');
     $result = DB::table('FormKK')->where('UserId', $session['UserId'])->get();
     return view('pelayanan.myListKK', compact('session', 'result'));
    }

    public function postKtp(Request $request)
    {
     $response = [];
     $response['status'] = 'error';
     $params = [];

     $params = [
      'UserId' => $request->UserId,
      'Provinsi' => $request->provinsi,
      'Kota' => $request->kota,
      'Kecamatan' => $request->kecamatan,
      'Kelurahan' => $request->kelurahan,
      'JenisPermohonan' => $request->checkbox,
      'NamaLengkap' => $request->nama,
      'NoKK' => $request->noKK,
      'NIK' => $request->nik,
      'Alamat' => $request->alamat,
      'RT' => $request->rt,
      'RW' => $request->rw,
      'KodePos' => $request->kodePos,
     ];

     // return $params;
     $insert = DB::table('FormKtp')->insert($params);
     if($insert) {
      $response['status'] = 'success';
      $response['message'] = 'Success Insert';
      $response['url'] = '/myListKtp';
     } else {
      $response['message'] = 'Failed Inserts';
      $response['url'] = '/ktp';
     }
     return response($response);
    }

    public function kk()
    {
     $session = session('user');
     return view('pelayanan/kk', compact('session'));
    }

    public function postKK(Request $request)
    {
     $response = [];
     $response['status'] = 'error';
     $params = [];

     $params = [
      'UserId' => $request->UserId,
      'Alamat' => $request->alamat,
      'alasan' => $request->alasan,
      'Provinsi' => $request->provinsi,
      'ProvinsiPemohon' => $request->provinsiPemohon,
      'Kabupaten' => $request->kabupaten,
      'KabupatenPemohon' => $request->kabupatenPemohon,
      'Kecamatan' => $request->kecamatan,
      'KecamatanPemohon' => $request->kecamatanPemohon,
      'Kelurahan' => $request->kelurahan,
      'KelurahanPemohon' => $request->kelurahanPemohon,
      'KodePos' => $request->kodepos,
      'NoKK' => $request->noKK,
      'Nik' => $request->nik,
      'RT' => $request->rt,
      'RW' => $request->rw,
      'Telepon' => $request->telepon,
      'Nama' => $request->nama
     ];

     $insert = DB::table('FormKK')->insert($params);
     if($insert) {
      $response['status'] = 'success';
      $response['message'] = 'Success Insert';
      $response['url'] = '/pelayanan';
     } else {
      $response['message'] = 'Failed Inserts';
      $response['url'] = '/kk';
     }
     return response($response);
    }

    public function nikah()
    {
     $session = session('user');
     return view('pelayanan/nikah', compact('session'));
    }

    public function postNikah(Request $request)
    {
     $response = [];
     $response['status'] = 'error';
     $params = [];

     $params = [
      'UserId' => $request->UserId,
      'NamaLengkap' => $request->nama,
      'JenisKelamin' => $request->jenisKelamain,
      'Pekerjaan' => $request->pekerjaan,
      'Status' => $request->status,
      'TempatLahir' => $request->tempat,
      'TanggalLahir' => $request->tanggalLahir,
      'TempatTinggal' => $request->tempatTinggal,
      'KeteranganPria' => $request->keteranganPria,
      'KeteranganWanita' => $request->keteranganWanita,
      'Agama' => $request->agama,
      'Bin' => $request->bin,
      'NamaPasanganLalu' => $request->sutriLalu,
      'Kua' => $request->kua,
      'Numpang' => $request->numpang
     ];

     $insert = DB::table('FormNikah')->insert($params);
     if($insert) {
      $response['status'] = 'success';
      $response['message'] = 'Success Insert';
      $response['url'] = '/pelayanan';
     } else {
      $response['message'] = 'Failed Inserts';
      $response['url'] = '/nikah';
     }
     return response($response);
    }

    public function lahir()
    {
     $session = session('user');
     return view('pelayanan/lahir', compact('session'));
    }

    public function postLahir(Request $request)
    {
     $response = [];
     $response['status'] = 'error';
     $params = [];

     $params = [
      'UserId' => $request->UserId,
      'NamaAyah' => $request->namaAyah,
      'NamaIbu' => $request->namaIbu,
      'UmurAyah' => $request->umurAyah,
      'UmurIbu' => $request->umurIbu,
      'PekerjaanAyah' => $request->pekerjaanAyah,
      'PekerjaanIbu' => $request->pekerjaanIbu,
      'AlamatAyah' => $request->alamatAyah,
      'AlamatIbu' => $request->alamatIbu,
      'KewarganegaraanAyah' => $request->kewarganegaraanAyah,
      'KewarganegaraanIbu' => $request->kewarganegaraanIbu,
      'KebangsaanAyah' => $request->kebangsaanAyah,
      'KebangsaanIbu' => $request->kebangsaanIbu,
      'ProvinsiAyah' => $request->provinsiAyah,
      'ProvinsiIbu' => $request->provinsiIbu,
      'KabupatenAyah' => $request->kabupatenAyah,
      'KabupatenIbu' => $request->kabupatenIbu,
      'KelurahanAyah' => $request->kelurahanAyah,
      'KelurahanIbu' => $request->kelurahanIbu,
      'KecamatanAyah' => $request->kecamatanAyah,
      'KecamatanIbu' => $request->kecamatanIbu,
      'TanggalKawin' => $request->tanggalKawin,
      'ProvinsiLahir' => $request->provinsiLahir,
      'KabupatenLahir' => $request->kabupatenLahir,
      'KelurahanLahir' => $request->kelurahanLahir,
      'KecamatanLahir' => $request->kecamatanLahir,
      'NamaKepalaKeluarga' => $request->namaKepalaKeluarga,
      'NomorKartuKeluarga' => $request->nomorKartuKeluarga,
      'NamaBayi' => $request->namaBayi,
      'BeratBayi' => $request->beratBayi,
      'PanjangBayi' => $request->panjangBayi,
      'JenisKelamin' => $request->jenisKelamin,
      'TempatDiLahirkan' => $request->tempatDiLahirkan,
      'TempatKelahiran' => $request->tempatKelahiran,
      'TanggalLahir' => $request->tanggalLahir,
      'Pukul' => $request->pukul,
      'KelahiranKe' => $request->kelahiranKe,
      'PenolongKelahiran' => $request->penolongKelahiran,
      'NikAyah' => $request->nikAyah,
      'NikIbu' => $request->nikIbu
     ];

     $insert = DB::table('FormLahir')->insert($params);
     if($insert) {
      $response['status'] = 'success';
      $response['message'] = 'Success Insert';
      $response['url'] = '/pelayanan';
     } else {
      $response['message'] = 'Failed Inserts';
      $response['url'] = '/kelahiran';
     }
     // return $params;
     return response($response);
    }

    public function mati()
    {
     $session = session('user');
     return view('pelayanan/mati',compact('session'));
    }

    public function postKematian(Request $request)
    {

     $response = [];
     $response['status'] = 'error';
     $params = [];

     $params = [
      'UserId' => $request->UserId,
      'NamaKepalaKeluarga' => $request->namaKepalaKeluarga,
      'NomorKepalaKeluarga' => $request->nomorKepalaKeluarga,

      'NikAyah' => $request->nikAyah,
      'NikIbu' => $request->nikIbu,
      'NikJenazah' => $request->nikJenazah,
      'NikPelapor' => $request->nikPelapor,

      'NamaAyah' => $request->namaAyah,
      'NamaIbu' => $request->namaIbu,
      'NamaPelapor' => $request->namaPelapor,
      'NamaJenazah' => $request->namaJenazah,

      'UmurAyah' => $request->umurAyah,
      'UmurIbu' => $request->umurIbu,
      'UmurPelapor' => $request->umurPelapor,
      'UmurJenazah' => $request->umurJenazah,

      'PekerjaanAyah' => $request->pekerjaanAyah,
      'PekerjaanIbu' => $request->pekerjaanIbu,
      'PekerjaanPelapor' => $request->pekerjaanPelapor,
      'PekerjaanJenazah' => $request->pekerjaanJenazah,

      'AlamatAyah' => $request->alamatAyah,
      'AlamatIbu' => $request->alamatIbu,
      'AlamatPelapor' => $request->alamatPelapor,
      'AlamatJenazah' => $request->alamatJenazah,

      'ProvinsiAyah' => $request->provinsiAyah,
      'ProvinsiIbu' => $request->provinsiIbu,
      'ProvinsiPelapor' => $request->provinsiPelapor,
      'ProvinsiJenazah' => $request->provinsiJenazah,

      'KabupatenAyah' => $request->kabupatenAyah,
      'KabupatenIbu' => $request->kabupatenIbu,
      'KabupatenPelapor' => $request->kabupatenPelapor,
      'KabupatenJenazah' => $request->kabupatenJenazah,

      'KelurahanAyah' => $request->kelurahanAyah,
      'KelurahanIbu' => $request->kelurahanIbu,
      'KelurahanPelapor' => $request->kelurahanPelapor,
      'KelurahanJenazah' => $request->kelurahanJenazah,

      'KecamatanAyah' => $request->kecamatanAyah,
      'KecamatanIbu' => $request->kecamatanIbu,
      'KecamatanPelapor' => $request->kecamatanPelapor,
      'KecamatanJenazah' => $request->kecamatanJenazah,

      'TempatLahirJenazah' => $request->tempatLahirJenazah,
      'TempatLahirAyah' => $request->tempatLahirAyah,
      'TempatLahirIbu' => $request->tempatLahirIbu,
      'TempatLahirPelapor' => $request->tempatLahirPelapor,

      'TanggalLahirAyah' => $request->tanggalLahirAyah,
      'TanggalLahirIbu' => $request->tanggalLahirIbu,
      'TanggalLahirPelapor' => $request->tanggalLahirPelapor,
      'TanggalLahirJenazah' => $request->tanggalLahirJenazah,

      'AgamaJenazah' => $request->agamaJenazah,
      'KelaminJenazah' => $request->kelaminJenazah,
      'Anak_Ke' => $request->anak_ke,
      'TanggalKematian' => $request->tanggalKematian,
      'Pukul' => $request->pukul,
      'SebabKematian' => $request->sebabKematian,
      'TempatKematian' => $request->tempatKematian,
      'YangMenerangkan' => $request->yangMenerangkan,
     ];

     $insert = DB::table('FormKematian')->insert($params);
     if($insert) {
      $response['status'] = 'success';
      $response['message'] = 'Success Insert';
      $response['url'] = '/pelayanan';
     } else {
      $response['message'] = 'Failed Inserts';
      $response['url'] = '/kelahiran';
     }

     return response($response);
    }

    public function deleteData(Request $request)
    {
     $response = [];
     $response['status'] = 'error';
     $params = [];
     $form = $request->type;
     $params = [
      'Id' => $request->id
     ];
     $delete = DB::table($form)->delete($params);
     if($delete) {
      $response['status'] = 'success';
      if ($form == 'FormKtp') {
       $response['url'] = '/myListKtp';
      }else if($form == 'FormKK'){
       $response['url'] = '/myListKK';
      }else if($form == 'FormNikah'){
       $response['url'] = '/myListNikah';
      }else if($form == 'FormLahir'){
       $response['url'] = '/myListLahir';
      }else if($form == 'FormKematian'){
       $response['url'] = '/myListKematian';
      }
      $response['message'] = 'Success Delete';
     }else{
      $response['message'] = 'Failed Delete';
      $response['url'] = '/pelayanan';
     }

     return response($response);
    }
}
