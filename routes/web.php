<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth::routes();
Route::get('/', function () {
 return view('site.login');
});

Route::get('login', 'AccountController@login')->middleware('userLogin');
Route::get('/logout', 'AccountController@logout');
Route::post('/doLogin', 'AccountController@doLogin');

Route::group(['middleware' => 'usersession'], function(){
 Route::get('/register', 'AccountController@register');
 Route::post('/doRegHead', 'AccountController@doRegHead');
 Route::post('/doRegFam', 'AccountController@doRegFam');
 Route::get('/registerKeluarga/{id}', 'AccountController@registerKeluarga');
 Route::get('/listUser', 'AccountController@listUser');
 Route::post('/deleteData', 'PelayananController@deleteData');

 // Print PDF
 Route::get('/pdfktp','ItemController@pdfktp');
 Route::get('/pdfkk','ItemController@pdfkk');
 Route::get('/pdfnikah','ItemController@pdfnikah');
 Route::get('/pdflahir','ItemController@pdflahir');
 Route::get('/pdfkematian','ItemController@pdfkematian');
 // Print Report
 Route::get('/printReport','ItemController@printReport');

 Route::get('/home', 'HomeController@index')->name('home');
 Route::get('/profile', 'ProfileController@index')->name('profile');
 // form ktp
 Route::get('/ktp', 'PelayananController@ktp')->name('ktp');
 Route::post('/postKtp', 'PelayananController@postKtp')->name('postKtp');
 Route::get('/listKtp', 'PelayananController@listKtp')->name('listKtp');
 Route::get('/myListKtp', 'PelayananController@myListKtp')->name('myListKtp');
 // form Kartu Keluarga
 Route::get('/kk', 'PelayananController@kk')->name('kk');
 Route::post('/postKK', 'PelayananController@postKK')->name('postKK');
 Route::get('/listKK', 'PelayananController@listKK')->name('listKK');
 Route::get('/myListKK', 'PelayananController@myListKK')->name('myListKK');
 // Form Kritik
 Route::get('/kritik', 'KritikController@index')->name('kritik');
 Route::post('/postKritik', 'KritikController@postKritik')->name('postKritik');
 // Form Nikah
 Route::get('/nikah', 'PelayananController@nikah')->name('nikah');
 Route::get('/listNikah', 'PelayananController@listNikah')->name('listNikah');
 Route::post('/postNikah', 'PelayananController@postNikah')->name('postNikah');
 Route::get('/myListNikah', 'PelayananController@myListNikah')->name('myListNikah');
 // Form Kelahiran
 Route::get('/kelahiran', 'PelayananController@lahir')->name('lahir');
 Route::get('/listKelahiran', 'PelayananController@listKelahiran')->name('listKelahiran');
 Route::post('/postLahir', 'PelayananController@postLahir')->name('postLahir');
 Route::get('/myListLahir', 'PelayananController@myListLahir')->name('myListLahir');
 // Form Kematian
 Route::get('/kematian', 'PelayananController@mati')->name('mati');
 Route::get('/listKematian', 'PelayananController@listKematian')->name('listKematian');
 Route::post('/postKematian', 'PelayananController@postKematian')->name('postKematian');
 Route::get('/myListKematian', 'PelayananController@myListKematian')->name('myListKematian');

 Route::get('/persyaratan', 'PersyaratanController@index')->name('persyaratan');
 Route::get('/kontak', 'KontakController@index')->name('kontak');

 Route::get('/laporan', 'LaporanController@index')->name('laporan');
 Route::get('/pelayanan', 'PelayananController@index')->name('pelayanan');


});
