<style type="text/css">
	table td, table th{
		border:1px solid black;
	}
</style>
<div class="container">

	<br/>
  <h1> TANDA BUKTI PENGISIAN FORM KTP</h1>
 <table class="table table-condensed">
   <tbody><tr>
     <th style="width: 10px">#</th>
     <th>Nama Lengkap</th>
     <th>Jenis Permohonan</th>
     <th>Alamat</th>
     <th>Tanggal Pengajuan</th>
     <th>Tanggal Selesai Pembuatan</th>
   </tr>
   <?php
   $i = 1;
   ?>
   @foreach($items as $key => $row)
   <?php
    ?>
   <tr>
     <td>{{$i++}}</td>
     <td>{{$row->NamaLengkap}}</td>
     <td>{{$row->JenisPermohonan}}</td>
     <td>{{$row->Alamat}}</td>
     <td><?= date("d-m-Y", strtotime($row->created_at))?></td>
     <td><?= date("d-m-Y", strtotime($row->created_at.'+ 7 days'))?></td>
     </td>
  </tr>
  @endforeach

 </tbody>
</table>
</div>
