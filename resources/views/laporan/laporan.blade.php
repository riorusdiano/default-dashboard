@extends('layouts.main')
@section('title-module')
  Laporan
@endsection

@section('content')
<div class="row">
  <div class="col-md-3 col-sm-6 col-12">
   <a href="{{ url('/listKtp')}}">
    <div class="info-box bg-info">
      <span class="info-box-icon"><i class="fa fa-files-o"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Form Permohonan KTP</span>
        <h2 class="info-box-number"><?= count($resultKtp) ?></h2>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
   </a>
  </div>
          <!-- /.col -->
   <div class="col-md-3 col-sm-6 col-12">
     <div class="info-box bg-success">
       <span class="info-box-icon"><i class="fa fa-files-o"></i></span>

       <div class="info-box-content">
         <span class="info-box-text">Form Permohonan KK</span>
         <h2 class="info-box-number"><?= count($resultKK) ?></h2>
       </div>
       <!-- /.info-box-content -->
     </div>
     <!-- /.info-box -->
   </div>
          <!-- /.col -->
   <div class="col-md-3 col-sm-6 col-12">
     <div class="info-box bg-warning">
       <span class="info-box-icon"><i class="fa fa-files-o"></i></span>

       <div class="info-box-content">
         <span class="info-box-text">Form Keterangan Nikah</span>
         <h2 class="info-box-number"><?= count($resultNikah) ?></h2>
       </div>
       <!-- /.info-box-content -->
     </div>
     <!-- /.info-box -->
   </div>
          <!-- /.col -->
   <div class="col-md-3 col-sm-6 col-12">
     <div class="info-box bg-danger">
       <span class="info-box-icon"><i class="fa fa-files-o"></i></span>

       <div class="info-box-content">
         <span class="info-box-text">Form Keterangan Kematian</span>
         <h2 class="info-box-number"><?= count($resultKematian) ?></h2>
       </div>
       <!-- /.info-box-content -->
     </div>
     <!-- /.info-box -->
   </div>

   <div class="col-md-3 col-sm-6 col-12">
     <div class="info-box bg-primary-gradient">
       <span class="info-box-icon"><i class="fa fa-files-o"></i></span>

       <div class="info-box-content">
         <span class="info-box-text">Form Keterangan Kelahiran</span>
         <h2 class="info-box-number"><?= count($resultKelahiran) ?></h2>
       </div>
       <!-- /.info-box-content -->
     </div>
     <!-- /.info-box -->
   </div>
   <!-- /.col -->
 </div>
@endsection
