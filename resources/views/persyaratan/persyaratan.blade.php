@extends('layouts.main')
@section('title-module')
  Persyaratan
@endsection

@section('content')
<div class="row">
          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  KTP ( Kartu Tanda Penduduk )
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               <p>Kartu identitas resmi penduduk sebagai bukti diri yang diterbitkan oleh Dinas yang berlaku di seluruh wilayah Negara Kesatuan Republik Indonesia.</p>
               <p>Persyaratan: </p>
                <ul>
                  <li>Pengantar dari RT / RW,</li>
                  <li>Fotocopy Kartu Keluarga (KK) 1 lembar</li>
                  <li>Mengisi blanko KTP</li>
                  <li>Melampirkan KTP asli bila perpanjangan</li>
                  <li>Bagi pemula wajib melakukan perekaman dengan melampirkan fotocopy akta kelahiran, ijasah terakhir (1 lembar)</li>
                  <li>Bukti pelunasan PBB tahun berjalan</li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- ./col -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  KK ( Kartu Keluarga )
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               <p>Kartu Keluarga adalah kartu identitas keluarga yang memuat data tentang nama, susunan dan hubungan dalam keluarga, serta identitas anggota keluarga.</p>
               <p>Persyaratan: </p>
               <ul>
                 <li>Pengantar dari RT / RW,</li>
                 <li>Mengisi blanko KK</li>
                 <li>Fotocopy akta nikah (2 lembar)</li>
                 <li>Fotocopy akta kelahiran dan ijazah dari masing-masing anggota keluarga (1 lembar)</li>
                 <li>Membawa Kartu Keluarga yang asli ( Jika sudah memiliki )</li>
                 <li>Tanda bukti Pelunasan PBB tahun berjalan</li>
               </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- ./col -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  Surat Keterangan Nikah
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               <p>Pelayanan Surat keterangan nikah adalah salah satu bentuk pelayanan publik dimana masyarakat mengajukan permohonan menikah  sesuai persyaratan yang berlaku</p>
               <p>Persyaratan: </p>
                <ul>
                  <li>Pengantar dari RT / RW</li>
                  <li>KTP dan KK asli</li>
                  <li>Akta kelahiran / ijasah asli</li>
                  <li>Facilisis in pretium nisl aliquet</li>
                  <li>Bukti pelunasan PBB tahun berjalan</li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  Surat Keterangan Kelahiran
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               <p>Pelayanan Surat Akta Kelahiran adalah salah satu bentuk pelayanan publik yang diselenggarakan Pemerintah dimana penduduk mendapatkan surat sebagai bukti keterangan telah melahirkan anak yang nantinya dapat digunakan untuk mengurus Akte Kelahiran.</p>

               <p>Persyaratan: </p>
                <ul>
                  <li>Pengantar dari RT / RW</li>
                  <li>KTP dan KK asli orang tua</li>
                  <li>Surat Nikah asli orang tua</li>
                  <li>Surat Keterangan Lahir dari Bidan atau Dokter</li>
                  <li>Bukti pelunasan PBB tahun berjalan</li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  Surat Keterangan Kematian
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               <p>Pelayanan Surat Keterangan Kematian adalah salah satu bentuk pelayanan publik yang diselenggarakan Pemerintah dimana penduduk mendapatkan surat sebagai bukti keterangan telah meninggal dunia yang nantinya dapat digunakan untuk mengurus asuransi atau tunjangan lainnya sesuai aturan yang berlaku.</p>

               <p>Persyaratan: </p>
                <ul>
                  <li>Pengantar dari RT / RW</li>
                  <li>Fotocopy KTP yang meninggal (1 lbr)</li>
                  <li>Fotocopy KK (1 lbr)</li>
                  <li>Bukti pelunasan PBB tahun berjalan</li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- ./col -->
        </div>
@endsection
