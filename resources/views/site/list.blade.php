@extends('layouts.main')
@section('title-module')
  List User
@endsection

@section('content')
<div class="row">
 <div class="col-md-12">
   <a href="{!!url('/register') !!}" class="btn btn-block btn-info btn-lg col-md-3">Create new user</a>
 </div>
</div>

<div class="row">
 <div class="col-md-12">

 <div class="card">
   <!-- /.card-header -->
   <div class="card-body p-0">
     <table class="table table-condensed">
       <tbody><tr>
         <th style="width: 10px">#</th>
         <th>Nama Kepala Keluarga</th>
         <th>NomorKartu</th>
         <th>Alamat</th>
         <th>Created At</th>
         <th style="width: 40px">Actions</th>
       </tr>
       <?php
       $i = 1;
       ?>
       @foreach($result as $key => $row)
       <tr>
          <td>{{$i++}}</td>
         <td>{{$row->KepalaKeluarga}}</td>
         <td>{{$row->NomorKartu}}</td>
         <td>{{$row->Alamat}}</td>
         <td><?= date("d-m-Y", strtotime($row->created_at))?></td>
         <td><span class="badge bg-danger">55%</span></td>
      </tr>
      @endforeach
     </tbody>
    </table>
   </div>
   <!-- /.card-body -->
 </div>
 <!-- /.card -->
</div>
</div>
@endsection
