
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>Registrasi</b>Keluarga</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Registersi Anggota Keluarga</p>

      <!-- <form action="{{url('/doRegFam')}}" method="post"> -->
      <form id="regisFam" method="POST">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <input id="id" type="hidden" name="id" value="{{ $id }}">
        <div class="form-group has-feedback">
          <input id="NamaLengkap" name="NamaLengkap"type="text" class="form-control" placeholder="Nama Lengkap">
        </div>
        <div class="form-group has-feedback">
          <input id="nik" name="nik" type="number" class="form-control" placeholder="NIK">
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-4">
            <button type="button" onclick="submitData()" class="btn btn-primary btn-block btn-flat">Register</button>
          </div>
          <div class="col-4">
            <a href="{{url('/home')}}" class="btn btn-danger btn-block btn-flat">Selesai
            </a>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>

<!-- /.register-box -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="{{ url('js/jquery.serializejson.js') }}" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
function successAlert() {
  swal("Success!", " Data Has Submit", "success");
 }

 function errorAlert(err = '') {
   swal("Error!", `Something Wrong, please confirm developer team

   ${err}
   `, "error");
   console.log(err);
  }

function submitData(){
 var dataByForm = $('#regisFam').serializeJSON();
 if(dataByForm.NamaLengkap.length == 0){
  alert('Nama tidak boleh kosong')
 }else if(dataByForm.nik.length == 0){
  alert('Nik tidak boleh kosong')
 }

 postAjax('{{ action('AccountController@doRegFam') }}',dataByForm,function(res){
   successAlert();
   if(res.status == 'error'){
     errorAlert();
   }

   $('#regisFam')[0].reset();

 });
}

function postAjax(url,data,callback){
    $('#loading').fadeIn(200);
    $.ajax({
        url: url,
        data: data,
        method:'POST',
        dataType: 'json',
        success: function(res) {
         $('#loading').fadeOut(200);
         if(callback){
          callback(res)
         }
        },
        error: function(xhr, status, error) {
          $('#loading').fadeOut(200);
          errorAlert(xhr.responseJSON.message);
        }
    });
  }
</script>
</body>
</html>
