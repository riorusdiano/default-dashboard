<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
  <!-- Font Awesome -->
</head>
<style media="screen">
@import url('https://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700');

.login{
  font-family: 'Josefin Sans', sans-serif;
  background: #fbfbfb;
  padding:40px 0px;
}

label{
   font-weight:400;
   font-size:15px;
}

.login-box{
   -webkit-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
   -moz-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
   box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
   padding:0px;
   background:#78d46e;
}

.left-box{
padding:50px;
color:#FFF;
}

.left-box h1{
font-weight:600;
   font-family: 'Josefin Sans', sans-serif;
   text-transform:capitalize;
   color:#FFF;
   font-size:32px;
}

.right-box{
background:#FFF;
min-height:520px;
}

.right-box h1{
font-weight:600;
   font-family: 'Josefin Sans', sans-serif;
   color:#444;
   font-size:32px;
   padding:50px;
}

.form{
   padding:20px 30px;
}

.form-control{
   box-shadow: none;
   border-radius: 0px;
   border-bottom: 1px solid #2196f3;
   border-top: 1px;
   border-left: none;
   border-right: none;
}

.btn{
   font-weight: 700;
   font-size:15px;
   color:#FFF;
   border-radius: 0;
   background: #78d46e;
   padding:12px 30px;
   float:right;
   margin-top:40px;
}

.btn:hover{
   border:2px solid #78d46e;
   background:#FFF;
}

input[type=text], input[type=password], input[type=email] {
   background-color: transparent;
   border: none;
   border-bottom: 1px solid #d2d2d2;
   border-radius: 0;
   margin-bottom:50px;
   box-shadow: none;
}

input[type=text]:focus, input[type=password]:focus, input[type=email]:focus {
   box-shadow: none;
   border-bottom: 1px solid #78d46e;
}

.form2 {
   padding:30px 0px;
}

.white-btn{
   background:#FFF;
   color:#78d46e;
}
</style>

<div class="login">
   	<div class="container">

		<div class="col-lg-8 col-lg-offset-2 login-box" >

    		<div class="col-lg-6 left-box">
        		<img style="width: 100%; height: 415px;" src="dist/img/logo.jpg" alt="">
      </div>
    		<div class="col-lg-6 right-box">
    		<h1>Login</h1>
        <form id="loginForm" method="post">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group has-feedback">
            <input id="nama" name="nama" type="text" class="form-control" placeholder="Nama Lengkap">
          </div>
          <div class="form-group has-feedback">
            <input id="noKK" name="noKK" type="password" class="form-control" placeholder="Nomor Kartu Keluarga">
          </div>
          <div class="row">
            <div class="col-8">

            </div>
            <!-- /.col -->
            <div class="col-md-12">
              <button type="button" onclick="submitData()" class="btn btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
	    	</div>  <!-- right-box -->
		</div> <!--col-lg-8-->

   </div>
</div>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="{{ url('js/jquery.serializejson.js') }}" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
function successAlert() {
  swal("Success!", " Berhasil Login", "success");
 }

 function errorAlert(err = '') {
   swal("Error!", `Something Wrong, please confirm developer team

   ${err}
   `, "error");
   console.log(err);
  }

function submitData(){
 var dataByForm = $('#loginForm').serializeJSON();
 if(dataByForm.nama.length == 0){
  alert('Nama tidak boleh kosong')
 }else if(dataByForm.noKK.length == 0){
  alert('Nomor kartu tidak boleh kosong')
 }

 postAjax('{{ action('AccountController@doLogin') }}',dataByForm,function(res){
   successAlert();
   window.location.replace(res.url)
   if(res.status == 'error'){
     errorAlert();
   }

   $('#loginForm')[0].reset();

 });
}

function postAjax(url,data,callback){
    // console.log(url,data, callback);
    $('#loading').fadeIn(200);
    $.ajax({
        url: url,
        data: data,
        method:'POST',
        dataType: 'json',
        success: function(res) {
         $('#loading').fadeOut(200);
         if(callback){
          callback(res)
         }
        },
        error: function(xhr, status, error) {
          $('#loading').fadeOut(200);
          errorAlert(xhr.responseJSON.message);
        }
    });
  }
</script>
