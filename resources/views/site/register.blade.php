<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <script>
     window.Laravel = <?php echo json_encode([
         'csrfToken' => csrf_token(),
     ]); ?>
  </script>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <style media="screen">
  #loading {
    width: 100%;
    height: 100%;
    background: 0 0;
    color: #fff;
    top: 0;
    position: fixed;
    z-index: 1000;
    display: none;
  }

  #loading-gif {
    border-radius: 5px;
    position: absolute;
    left: 50%;
    margin-left: -20px;
    top: 50%;
    margin-top: -20px;
    text-align: center;
  }

</style>

</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>Registrasi</b> Keluarga</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Silahkan lengkapi data dibawah ini</p>
      <!-- <form action="{{url('/doRegHead')}}" method="post"> -->
      <form id="regisHead" method="POST">
       {{ csrf_field() }}
        <div class="form-group has-feedback">
          <input id="no_kk" name="no_kk" type="number" class="form-control" placeholder="Nomor Kartu Keluarga" required onfocus="true">
        </div>
        <div class="form-group has-feedback">
          <input id="nama_kepala_keluarga" name="nama_kepala_keluarga" type="text" class="form-control" placeholder="Nama Kepala Keluarga">
        </div>
        <div class="form-group has-feedback">
          <textarea id="alamat" name="alamat" rows="5" cols="38" placeholder="   Alamat"></textarea>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-4">
            <button type="button" onclick="doRegis()" class="btn btn-primary btn-block btn-flat">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->
<!-- Sweet Alert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
function successAlert() {
  swal("Success!", " Data Has Submit", "success");
 }

 function errorAlert(err = '') {
   swal("Error!", `Something Wrong, please confirm developer team

   ${err}
   `, "error");
   console.log(err);
  }

  doRegis = () => {
   let params = [];
   let NomorKartu = $('#no_kk').val();
   let NamaKepala = $('#nama_kepala_keluarga').val();
   let Alamat = $('#alamat').val();
   var token_csrf = $('input[name="_token"]').val();
   // if(NomorKartu.length < 16){
   //  alert('Nomor kartu keluarga salah')
   // }
   params = { NomorKartu:NomorKartu,
              KepalaKeluarga:NamaKepala,
              Alamat:Alamat,
              _token:token_csrf
            }
  $('#loading').fadeIn(200);
  $.ajax({
    url: "doRegHead",
    method: "POST",
    data: params,
    success: function(data) {
     console.log(data.id);
       $('#loading').fadeOut(200);
        successAlert();
        window.location.replace('/registerKeluarga/'+data.id)
      },
    });
  }
</script>
</body>
</html>
