<?php
$session= session('user');
?>
<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Riset Penelitian</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">
           {{ $session['KepalaKeluarga'] }}
          </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="home" class="{{Request::path() == 'home' ? 'nav-link active' : 'nav-link' }}">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
                <!-- <i class="right fa fa-angle-left"></i> -->
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="profile" class="{{Request::path() == 'profile' ? 'nav-link active' : 'nav-link' }}">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Profile
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="persyaratan" class="{{Request::path() == 'persyaratan' ? 'nav-link active' : 'nav-link' }}">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Persyaratan
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="pelayanan" class="{{Request::path() == 'pelayanan' ? 'nav-link active' : 'nav-link' }}">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Menu Pelayanan
              </p>
            </a>
            </li>
          <li class="nav-header">REPORT</li>
          <?php
           ?>
           @if($session['Role'] != 'user')
          <li class="nav-item">
            <a href="laporan" class="{{Request::path() == 'laporan' ? 'nav-link active' : 'nav-link' }}">
              <i class="nav-icon fa fa-info"></i>
              <p>
                Laporan
              </p>
            </a>
          </li>
          @endif
          @if($session['Role'] != 'admin')
          <li class="nav-item has-treeview">
            <a href="kritik" class="{{Request::path() == 'kritik' ? 'nav-link active' : 'nav-link' }}">
              <i class="nav-icon fa fa-envelope-o"></i>
              <p>
                Kritik & Saran
              </p>
            </a>
          </li>
          @endif

          <li class="nav-item has-treeview">
            <a href="kontak" class="{{Request::path() == 'kontak' ? 'nav-link active' : 'nav-link' }}">
              <i class="nav-icon fa fa-phone"></i>
              <p>
                Kontak
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="{{Request::path() == '' ? 'nav-link active' : 'nav-link' }}">
              <i class="nav-icon fa fa-gear"></i>
              <p>
                Pengaturan
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: none;">
             @if($session['Role'] == 'admin')
              <li class="nav-item">
                <a href="{!! url('/listUser') !!}" class="nav-link active">
                  <i class="fa fa-user"></i>
                  <p>&nbsp; Kelola User</p>
                </a>
              </li>
             @endif
              <li class="nav-item">
                <a href="/logout" class="nav-link">
                  <i class="fa fa-sign-out"></i>
                  <p>&nbsp; Log Out</p>
                </a>
              </li>
            </ul>
          </li>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
