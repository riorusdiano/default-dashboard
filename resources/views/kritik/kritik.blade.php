@extends('layouts.main')
@section('title-module')
  Kritik & Saran
@endsection

@section('content')
<div class="row">
 <section class="col-lg-12 connectedSortable ui-sortable">
  <!-- Custom tabs (Charts with tabs)-->
   <div class="card">
     <div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
       <h3 class="card-title p-3">Kritik & Saran</h3>
     </div><!-- /.card-header -->
     <div class="card-body">
      <form id="formKritik" method="post">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
         <label>Nama : {{$session['KepalaKeluarga']}}
          <input id="name" name="name" type="hidden" value="{{$session['KepalaKeluarga']}}">
         </label>
          <input id="UserId" name="UserId" type="hidden" value="{{$session['UserId']}}">
        </div>
        <div class="form-group">
         <label>Pesan</label>
         <textarea id="kritik" name="kritik" class="form-control" rows="3" placeholder="Masukan pesan anda"></textarea>
        </div>
        <button class="btn btn-success" type="button" onclick="submitData()" name="button">Kirim</button>
      </form>
     </div>
    </div><!-- /.card-body -->
 </section>
</div>
@endsection
<script type="text/javascript">
function submitData(){
 var dataByForm = $('#formKritik').serializeJSON();
 postAjax('{{ action('KritikController@postKritik') }}',dataByForm,function(res){
   successAlert();
   window.location.replace(res.url)
   if(res.status == 'error'){
     errorAlert();
   }
   $('#formKritik')[0].reset();
 });
}

function postAjax(url,data,callback){
    $('#loading').fadeIn(200);
    $.ajax({
        url: url,
        data: data,
        method:'POST',
        dataType: 'json',
        success: function(res) {
         console.log(res);
         $('#loading').fadeOut(200);
         if(callback){
          callback(res)
         }
        }
        // error: function(xhr, status, error) {
        //   $('#loading').fadeOut(200);
        //   errorAlert(xhr.responseJSON.message);
        // }
    });
  }
</script>
