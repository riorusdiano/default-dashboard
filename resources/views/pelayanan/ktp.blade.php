@extends('layouts.main')
@section('title-module')
  Form Permohonan KTP
@endsection

@section('content')
<div class="row">
 <section class="col-lg-12 connectedSortable ui-sortable">
  <!-- Custom tabs (Charts with tabs)-->
   <div class="card">
     <div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
       <h3 class="card-title p-3">Form Permohonan Kartu Tanda Penduduk</h3>
     </div><!-- /.card-header -->
     <div class="card-body">
      <div class="row">
      <div class="col-lg-12">
      <form id="formKtp" method="post">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input id="UserId" name="UserId" type="hidden" value="{{$session['UserId']}}">
        <div class="form-group">
         <div class="row">
          <div class="col-lg-6">
           <label>Provinsi</label>
           <input id="provinsi" type="text" class="form-control mb-10 form-reset" name="provinsi" placeholder="Provinsi" value="" required>
          </div>
          <div class="col-lg-6">
           <label>Kabupaten / Kota</label>
           <input id="kota" type="text" class="form-control mb-10 form-reset" name="kota" placeholder="Kabupaten / Kota" value="" required>
          </div>
         </div>
       </div>

        <div class="form-group">
         <div class="row">
          <div class="col-lg-6">
           <label>Kecamatan</label>
           <input id="kecamatan" type="text" class="form-control mb-10 form-reset" name="kecamatan" placeholder="Kecamatan" value="" required>
          </div>
          <div class="col-lg-6">
           <label>Kelurahan</label>
           <input id="kelurahan" type="text" class="form-control mb-10 form-reset" name="kelurahan" placeholder="Kelurahan" value="" required>
          </div>
         </div>
       </div>

        <div class="form-group">
         <label>Permohonan KTP</label>
          <div class="form-check">
           <div class="row">
            <div class="col-md-4">
             <input id="checkbox" name="checkbox" class="form-check-input" type="radio" value="Baru">
             <label class="form-check-label">Baru</label>
            </div>
            <div class="col-md-4">
             <input id="checkbox" name="checkbox" class="form-check-input" type="radio" value="Perpanjangan">
             <label class="form-check-label">Perpanjangan</label>
            </div>
            <div class="col-md-4">
             <input id="checkbox" name="checkbox" class="form-check-input" type="radio" value="Pergantian">
             <label class="form-check-label">Pergantian</label>
            </div>
           </div>
          </div>
        </div>


        <div class="form-group">
         <label>Nama Lengkap</label>
         <input id="nama" type="text" class="form-control mb-10 form-reset" name="nama" placeholder="Nama Lengkap" value="" required>
        </div>
        <div class="form-group">
         <div class="row">
          <div class="col-lg-6">
           <label>No KK</label>
           <input id="noKK" type="number" class="form-control mb-10 form-reset" name="noKK" placeholder="Nomor Kartu Keluarga" value="" required>
          </div>
          <div class="col-lg-6">
           <label>NIK</label>
           <input id="nik" type="number" class="form-control mb-10 form-reset" name="nik" placeholder="NIK" value="" required>
          </div>
         </div>
        </div>

        <div class="form-group">
         <div class="row">
          <div class="col-lg-4">
           <label>Alamat</label>
           <br>
           <textarea id="alamat" type="text" name="alamat" rows="8" cols="80" required></textarea>
          </div>
           <div style="margin-left: 237px;" class="col-lg-1">
            <label>RT</label>
            <input id="rt" type="number" class="form-control mb-10 form-reset check_class" name="rt" placeholder="RT" value="" required>
           </div>
           <div class="col-lg-1">
            <label>RW</label>
            <input id="rw" type="number" class="form-control mb-10 form-reset check_class" name="rw" placeholder="RW" value="" required>
           </div>
           <div class="col-lg-2">
            <label>Kode Pos</label>
            <input id="kodePos" type="number" class="form-control mb-10 form-reset check_class" name="kodePos" placeholder="Kode Pos" value="" required>
           </div>
          </div>
         </div>

        </div>
       <button class="btn btn-success" type="button" name="button" onclick="submitData()">Ajukan</button>
      </form>
     </div>
    </div><!-- /.card-body -->

   </div>
  </div>
 </section>
</div>
@endsection
<script type="text/javascript">
function submitData(){
 let dataByForm = $('#formKtp').serializeJSON();

 // FUNGSI CONSOLE LOG UNTUK VARIABEL "dataByForm" isinya sesuai atau tidak
 console.log(dataByForm);
 // Dijadikan dalam suatu objek
 let {alamat, kecamatan, kelurahan, kodePos, kota, nama, nik, noKK, provinsi, rt, rw } = dataByForm;

 switch (true) {
  case (alamat == ''):
   alert('silahkan isi alamat')
   break;
  case (kecamatan == ''):
   alert('silahkan isi kecamatan')
   break;
  case (kelurahan == ''):
   alert('silahkan isi kelurahan')
   break;
  case (kodePos == ''):
   alert('silahkan isi kode pos')
   break;
  case (kota == ''):
   alert('silahkan isi nama kota')
   break;
  case (nama == ''):
   alert('silahkan isi nama lengkap')
   break;
  case (nik == ''):
   alert('silahkan isi nik')
   break;
  case (noKK == ''):
   alert('silahkan isi nomor Kartu Keluarga')
   break;
  case (provinsi ==''):
   alert('silahkan isi provinsi')
   break;
  case (rt ==''):
   alert('silahkan isi rt')
   break;
  case (rw == ''):
   alert('silahkan isi rw')
   break;
  default:
  postAjax('{{ action('PelayananController@postKtp') }}',dataByForm,function(res){
   successAlert();
   window.location.replace(res.url)
   if(res.status == 'error'){
    errorAlert();
   }
   $('#formKtp')[0].reset();
  });
 }
}

function postAjax(url,data,callback){
    $('#loading').fadeIn(200);
    $.ajax({
        url: url,
        data: data,
        method:'POST',
        dataType: 'json',
        success: function(res) {
         console.log(res);
         $('#loading').fadeOut(200);
         if(callback){
          callback(res)
         }
        }
        // error: function(xhr, status, error) {
        //   $('#loading').fadeOut(200);
        //   errorAlert(xhr.responseJSON.message);
        // }
    });
  }
</script>
