@extends('layouts.main')
@section('title-module')
  List Pemohon KTP
@endsection

@section('content')
<div class="row">
 <div class="col-md-12">
 </div>
</div>

<div class="row">
 <div class="col-md-12">

 <div class="card">
   <!-- /.card-header -->
   <div class="card-body p-0">
     <table class="table table-condensed">
       <tbody>
        <tr>
         <th style="width: auto;">#</th>
         <th >Nama Lengkap</th>
         <th>Jenis Permohonan</th>
         <th>Alamat</th>
         <th>Created At</th>
       </tr>
       <?php
       $i = 1;
       ?>
       @foreach($result as $key => $row)
       <tr>
         <td>{{$i++}}</td>
         <td>{{$row->NamaLengkap}}</td>
         <td>{{$row->JenisPermohonan}}</td>
         <td>{{$row->Alamat}}</td>
         <td><?= date("d-m-Y", strtotime($row->created_at))?></td>
      </tr>
      @endforeach
     </tbody>
    </table>
   </div>
   <!-- /.card-body -->
 </div>
 <!-- /.card -->
 <button onclick="printForm()" class="btn btn-block btn-success btn-lg col-md-1">
  <span><i class="fa fa-print"></i></span> &nbsp; Print
</button>
 </div>
</div>
@endsection
<script type="text/javascript">
function printForm(){
 swal('Are you sure print this report?', {
      buttons: {
        cancel: 'Cancel',
        catch: {
          text: 'Print Report',
          value: 'print',
        },
      },
    }).then((value) => {
          switch (value) {
            case 'print':
            let data = {type: 'FormKtp'};
            printAjax('{{ action('ItemController@printReport')}}', data, function(res){
             // console.log(res);
            });
              break;
            default:
              swal('Cancel print ticket');
              break;
          }
      });
}

function printAjax(url, data, callback){
 $('#loading').fadeIn(200);
 $.ajax({
     url: url,
     data: data,
     method:'GET',
     contentType: false,
     xhrFields: {
               responseType: 'blob'
           },
     success: function(response, status, xhr) {
      var filename = "";
        var disposition = xhr.getResponseHeader('Content-Disposition');
         if (disposition) {
            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            var matches = filenameRegex.exec(disposition);
            if (matches !== null && matches[1]) filename = matches[1].replace(/['"]/g, '');
        }
        var linkelem = document.createElement('a');
        try {
         var blob = new Blob([response], { type: 'application/octet-stream' });
         if (typeof window.navigator.msSaveBlob !== 'undefined') {
             //   IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
             window.navigator.msSaveBlob(blob, filename);
         } else {
             var URL = window.URL || window.webkitURL;
             var downloadUrl = URL.createObjectURL(blob);

             if (filename) {
                 // use HTML5 a[download] attribute to specify filename
                 var a = document.createElement("a");

                 // safari doesn't support this yet
                 if (typeof a.download === 'undefined') {
                     window.location = downloadUrl;
                 } else {
                     a.href = downloadUrl;
                     a.download = filename;
                     document.body.appendChild(a);
                     a.target = "_blank";
                     a.click();
                 }
             } else {
                 window.location = downloadUrl;
             }
         }
        } catch (ex) {
       }
     }
 });
}
</script>
