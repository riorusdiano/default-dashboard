@extends('layouts.main')
@section('title-module')
  Form Permohonan KK
@endsection

@section('content')
<div class="row">
 <section class="col-lg-12 connectedSortable ui-sortable">
  <!-- Custom tabs (Charts with tabs)-->
   <div class="card">
     <div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
       <h3 class="card-title p-3">Form Permohonan Kartu Keluarga</h3>
     </div><!-- /.card-header -->
     <div class="card-body">
      <div class="col-lg-12">
      <form id="formKK" method="post">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <input id="UserId" name="UserId" type="hidden" value="{{$session['UserId']}}">

       <div class="form-group">
        <div class="row">
         <div class="col-lg-6">
          <label>Provinsi</label>
          <input id="provinsi" type="text" class="form-control mb-10 form-reset" name="provinsi" placeholder="Provinsi" value="">
         </div>
         <div class="col-lg-6">
          <label>Kabupaten / Kota</label>
          <input id="kabupaten" type="text" class="form-control mb-10 form-reset" name="kabupaten" placeholder="Kabupaten / Kota" value="">
         </div>
        </div>
      </div>

      <div class="form-group">
       <div class="row">
        <div class="col-lg-6">
         <label>Kecamatan</label>
         <input id="kecamatan" type="text" class="form-control mb-10 form-reset" name="kecamatan" placeholder="Kecamatan" value="">
        </div>
        <div class="col-lg-6">
         <label>Kelurahan</label>
         <input id="kelurahan" type="text" class="form-control mb-10 form-reset" name="kelurahan" placeholder="Kelurahan" value="">
        </div>
       </div>
     </div>

     <div class="form-group">
      <label>Nama Lengkap Pemohon</label>
      <input id="nama" type="text" class="form-control mb-10 form-reset" name="nama" placeholder="Nama Lengkap" value="<?=isset($detail) ? $detail['title'] : ''?>">
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>No KK Semula</label>
        <input id="noKK" type="number" class="form-control mb-10 form-reset" name="noKK" placeholder="Nomor Kartu Keluarga" value="">
       </div>
       <div class="col-lg-6">
        <label>NIK</label>
        <input id="nik" type="number" class="form-control mb-10 form-reset" name="nik" placeholder="NIK" value="">
       </div>
      </div>
     </div>

     <br/>
     <hr/>
      <h1>Alamat Pemohon</h1>
     <hr/>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-4">
        <label>Alamat</label>
        <br>
        <textarea id="alamat" name="alamat" rows="8" cols="80"></textarea>
       </div>
       <div class="row">
        <div style="margin-left: 277px;" class="col-lg-2">
         <label>RT</label>
         <input id="rt" type="number" class="form-control mb-10 form-reset" name="rt" placeholder="RT" value="">
         <br/>
         <label>Telepon</label>
         <input id="telepon" type="number" class="form-control mb-10 form-reset" name="telepon" placeholder="Nomor Telepon" value="">
        </div>
        <div class="col-lg-2">
         <label>RW</label>
         <input id="rw" type="number" class="form-control mb-10 form-reset" name="rw" placeholder="RW" value="">
        </div>
        <div class="col-lg-3">
         <label>Kode Pos</label>
         <input id="kodepos" type="number" class="form-control mb-10 form-reset" name="kodepos" placeholder="Kode Pos" value="">
        </div>
        </div>
       </div>
      </div>

      <div class="form-group">
       <div class="row">
        <div class="col-lg-6">
         <label>Provinsi </label>
         <input id="provinsiPemohon" type="text" class="form-control mb-10 form-reset" name="provinsiPemohon" placeholder="Provinsi" value="<?=isset($detail) ? $detail['title'] : ''?>">
        </div>
        <div class="col-lg-6">
         <label>Kabupaten / Kota</label>
         <input id="kabupatenPemohon" type="text" class="form-control mb-10 form-reset" name="kabupatenPemohon" placeholder="Kabupaten / Kota" value="<?=isset($detail) ? $detail['title'] : ''?>">
        </div>
       </div>
      </div>

      <div class="form-group">
       <div class="row">
        <div class="col-lg-6">
         <label>Kecamatan</label>
         <input id="kecamatanPemohon" type="text" class="form-control mb-10 form-reset" name="kecamatanPemohon" placeholder="Kecamatan" value="">
        </div>
        <div class="col-lg-6">
         <label>Kelurahan</label>
         <input id="kelurahanPemohon" type="text" class="form-control mb-10 form-reset" name="kelurahanPemohon" placeholder="Kelurahan" value="">
        </div>
       </div>
     </div>

     <div class="form-group">
      <label>Alasan Pemohon</label>
       <div class="form-check">
        <div class="row">
         <div class="col-md-4">
          <input id="alasan" class="form-check-input" name="alasan" type="radio" value="Rumah Tangga Baru">
          <label class="form-check-label">Karena Membentuk Rumah Tangga Baru</label>
         </div>
         <div class="col-md-4">
          <input id="alasan" class="form-check-input" name="alasan" type="radio" value="Hilang">
          <label class="form-check-label">Karena Karena Kartu Keluarga Hilang / Rusak</label>
         </div>
         <div class="col-md-4">
          <input id="alasan" class="form-check-input" name="alasan" type="radio" value="Lainnya">
          <label class="form-check-label">Lainnya</label>
         </div>
        </div>
       </div>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>Jumlah Anggota Keluarga</label>
        <input id="jumlah" type="number" class="form-control mb-10 form-reset" name="kecamatan" placeholder="Jumlah Anggota Keluarga" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
      </div>
     </div>

      <button class="btn btn-success" type="button" onclick="submitData()" name="button">Simpan</button>
     </form>
    </div>
   </div><!-- /.card-body -->
  </div>
 </section>
</div>
@endsection
<script type="text/javascript">
function submitData(){
 var dataByForm = $('#formKK').serializeJSON();
 console.log(dataByForm);
 postAjax('{{ action('PelayananController@postKK') }}',dataByForm,function(res){
   successAlert();
   window.location.replace(res.url)
   if(res.status == 'error'){
     errorAlert();
   }
   $('#formKK')[0].reset();
 });
}

function postAjax(url,data,callback){
    $('#loading').fadeIn(200);
    $.ajax({
        url: url,
        data: data,
        method:'POST',
        dataType: 'json',
        success: function(res) {
         console.log(res);
         $('#loading').fadeOut(200);
         if(callback){
          callback(res)
         }
        },
        error: function(xhr, status, error) {
          $('#loading').fadeOut(200);
          errorAlert(xhr.responseJSON.message);
        }
    });
  }
</script>
