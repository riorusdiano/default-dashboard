@extends('layouts.main')
@section('title-module')
  Form Keterangan Kelahiran
@endsection

@section('content')
<div class="row">
 <section class="col-lg-12 connectedSortable ui-sortable">
  <!-- Custom tabs (Charts with tabs)-->
   <div class="card">
     <div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
       <h3 class="card-title p-3">Form Keterangan Kelahiran</h3>
     </div><!-- /.card-header -->
     <div class="card-body">
      <div class="col-lg-12">
      <form id="formLahir" method="post">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <input id="UserId" name="UserId" type="hidden" value="{{$session['UserId']}}">
       <div class="form-group">
        <div class="row">
         <div class="col-lg-6">
          <label>Provinsi</label>
          <input id="provinsiLahir"type="text" class="form-control mb-10 form-reset" name="provinsiLahir" placeholder="Provinsi">
         </div>
         <div class="col-lg-6">
          <label>Kabupaten / Kota</label>
          <input id="kabupatenLahir" type="text" class="form-control mb-10 form-reset" name="kabupatenLahir" placeholder="Kabupaten / Kota">
         </div>
        </div>
      </div>

      <div class="form-group">
       <div class="row">
        <div class="col-lg-6">
         <label>Kecamatan</label>
         <input id="kecamatanLahir"type="text" class="form-control mb-10 form-reset" name="kecamatanLahir" placeholder="Kecamatan">
        </div>
        <div class="col-lg-6">
         <label>Kelurahan</label>
         <input id="kelurahanLahir"type="text" class="form-control mb-10 form-reset" name="kelurahanLahir" placeholder="Kelurahan">
        </div>
       </div>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>Nama Kepala Keluarga</label>
        <input id="namaKepalaKeluarga" type="text" class="form-control mb-10 form-reset" name="namaKepalaKeluarga" placeholder="Nama Lengkap" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
       <div class="col-lg-6">
        <label>Nomor Kartu Keluarga</label>
        <input id="nomorKartuKeluarga" type="number" class="form-control mb-10 form-reset" name="nomorKartuKeluarga" placeholder="Nomor Kartu Keluarga" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
      </div>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>Nama Bayi / Anak </label>
        <input id="namaBayi" type="text" class="form-control mb-10 form-reset" name="namaBayi" placeholder="Nama Lengkap" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
       <div class="col-lg-6">
        <label>Jenis Kelamin</label>
        <select id="jenisKelamin" name="jenisKelamin" class="form-control" data-placeholder="Select a State" style="width: 100%;" tabindex="-1" aria-hidden="true">
          <option selected>Silahkan Pilih</option>
          <option>Laki-Laki</option>
          <option>Perempuan</option>
        </select>
       </div>
     </div>
    </div>

    <div class="form-group">
     <label>Tempat Dilahirkan</label>
      <div class="form-check">
       <div class="row">
        <div class="col-md-2">
         <input id="tempatDiLahirkan" name="tempatDiLahirkan" class="form-check-input" type="radio" value="Rs/Rb">
         <label class="form-check-label">Rs / Rb</label>
        </div>
        <div class="col-md-2">
         <input id="tempatDiLahirkan" name="tempatDiLahirkan" class="form-check-input" type="radio" value="Puskesmas">
         <label class="form-check-label">Puskesmas</label>
        </div>
        <div class="col-md-2">
         <input id="tempatDiLahirkan" name="tempatDiLahirkan" class="form-check-input" type="radio" value="Polindes">
         <label class="form-check-label">Polindes</label>
        </div>
        <div class="col-md-2">
         <input id="tempatDiLahirkan" name="tempatDiLahirkan" class="form-check-input" type="radio" value="Rumah">
         <label class="form-check-label">Rumah</label>
        </div>
        <div class="col-md-2">
         <input id="tempatDiLahirkan" name="tempatDiLahirkan" class="form-check-input" type="radio" value="Lainnya">
         <label class="form-check-label">Lainnya</label>
        </div>
       </div>
      </div>
    </div>

    <div class="form-group">
     <div class="row">
      <div class="col-lg-4">
       <label>Tempat Kelahiran</label>
       <input id="tempatKelahiran" type="text" class="form-control mb-10 form-reset" name="tempatKelahiran" placeholder="Tempat Lahir" value="<?=isset($detail) ? $detail['title'] : ''?>">
      </div>
      <div class="col-lg-4">
        <label>Tanggal Lahir</label>
        <div class="input-group">
        <div class="input-group-prepend">
         <span class="input-group-text"><i class="fa fa-calendar"></i></span>
        </div>
        <input id="tanggalLahir" name="tanggalLahir" type="text" class="form-control mb-10 form-reset" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
       </div>
      </div>
      <div class="col-lg-4">
       <label>Pukul</label>
       <input id="pukul" type="number" class="form-control mb-10 form-reset" name="pukul" placeholder="Pukul" value="<?=isset($detail) ? $detail['title'] : ''?>">
      </div>
     </div>
    </div>

    <div class="form-group">
     <div class="row">
      <div class="col-lg-6">
       <label>Kelahiran Ke-</label>
       <input id="kelahiranKe" type="number" class="form-control mb-10 form-reset" name="kelahiranKe" placeholder="Kelahiran Ke" value="<?=isset($detail) ? $detail['title'] : ''?>">
      </div>
      <div class="col-lg-6">
       <label>Penolong Kelahiran</label>
       <input id="penolongKelahiran" type="text" class="form-control mb-10 form-reset" name="penolongKelahiran" placeholder="Penolong Kelahiran" value="<?=isset($detail) ? $detail['title'] : ''?>">
      </div>
     </div>
    </div>


    <div class="form-group">
     <div class="row">
      <div class="col-lg-6">
       <label>Berat Bayi</label>
       <input id="beratBayi" type="number" class="form-control mb-10 form-reset" name="beratBayi" placeholder="Berat Bayi" value="<?=isset($detail) ? $detail['title'] : ''?>">
      </div>
      <div class="col-lg-6">
       <label>Panjang Bayi</label>
       <input id="panjangBayi" type="number" class="form-control mb-10 form-reset" name="panjangBayi" placeholder="Panjang Bayi" value="<?=isset($detail) ? $detail['title'] : ''?>">
      </div>
     </div>
    </div>

    <hr>
    <h1>Data Ayah</h1>
    <hr>

    <div class="form-group">
     <div class="row">
      <div class="col-lg-6">
       <label>NIK</label>
       <input id="nikAyah" type="number" class="form-control mb-10 form-reset" name="nikAyah" placeholder="NIK">
      </div>
      <div class="col-lg-6">
       <label>Nama Lengkap</label>
       <input id="namaAyah" type="text" class="form-control mb-10 form-reset" name="namaAyah" placeholder="Nama Lengkap">
      </div>
     </div>
    </div>

    <div class="form-group">
     <div class="row">
      <div class="col-lg-6">
       <label>Umur</label>
       <input id="umurAyah" type="number" class="form-control mb-10 form-reset" name="umurAyah" placeholder="Umur">
      </div>
      <div class="col-lg-6">
       <label>Pekerjaan</label>
       <input id="pekerjaanAyah" type="text" class="form-control mb-10 form-reset" name="pekerjaanAyah" placeholder="Pekerjaan">
      </div>
     </div>
    </div>

    <div class="form-group">
     <label>Alamat</label>
     <br>
     <textarea id="alamatAyah" name="alamatAyah" rows="8" cols="80"></textarea>
    </div>

    <div class="form-group">
     <div class="row">
      <div class="col-lg-6">
       <label>Provinsi</label>
       <input id="provinsiAyah" type="text" class="form-control mb-10 form-reset" name="provinsiAyah" placeholder="Provinsi">
      </div>
      <div class="col-lg-6">
       <label>Kabupaten / Kota</label>
       <input id="kabupatenAyah"type="text" class="form-control mb-10 form-reset" name="kabupatenAyah" placeholder="Kabupaten / Kota">
      </div>
     </div>
   </div>

   <div class="form-group">
    <div class="row">
     <div class="col-lg-6">
      <label>Kecamatan</label>
      <input id="kecamatanAyah" type="text" class="form-control mb-10 form-reset" name="kecamatanAyah" placeholder="Kecamatan">
     </div>
     <div class="col-lg-6">
      <label>Kelurahan</label>
      <input id="kelurahanAyah" type="text" class="form-control mb-10 form-reset" name="kelurahanAyah" placeholder="Kelurahan">
     </div>
    </div>
  </div>

  <div class="form-group">
   <div class="row">
    <div class="col-lg-6">
     <label>Kewarganegaraan</label>
     <input id="kewarganegaraanAyah" type="text" class="form-control mb-10 form-reset" name="kewarganegaraanAyah" placeholder="Kewarganegaraan">
    </div>
    <div class="col-lg-6">
     <label>Kebangsaan</label>
     <input id="kebangsaanAyah" type="text" class="form-control mb-10 form-reset" name="kebangsaanAyah" placeholder="Kebangsaan">
    </div>
   </div>
 </div>

 <hr>
  <h1>Data Ibu</h1>
 <hr>
 <div class="form-group">
  <div class="row">
   <div class="col-lg-6">
    <label>NIK</label>
    <input id="nikIbu" type="number" class="form-control mb-10 form-reset" name="nikIbu" placeholder="NIK">
   </div>
   <div class="col-lg-6">
    <label>Nama Lengkap</label>
    <input id="namaIbu" type="text" class="form-control mb-10 form-reset" name="namaIbu" placeholder="Nama Lengkap">
   </div>
  </div>
 </div>

 <div class="form-group">
  <div class="row">
   <div class="col-lg-6">
    <label>Umur</label>
    <input id="umurIbu" type="number" class="form-control mb-10 form-reset" name="umurIbu" placeholder="Umur">
   </div>
   <div class="col-lg-6">
    <label>Pekerjaan</label>
    <input id="pekerjaanIbu"type="text" class="form-control mb-10 form-reset" name="pekerjaanIbu" placeholder="Pekerjaan">
   </div>
  </div>
 </div>

 <div class="form-group">
  <label>Alamat</label>
  <br>
  <textarea id="alamatIbu" name="alamatIbu" rows="8" cols="80"></textarea>
 </div>

 <div class="form-group">
  <div class="row">
   <div class="col-lg-6">
    <label>Provinsi</label>
    <input id="provinsiIbu"type="text" class="form-control mb-10 form-reset" name="provinsiIbu" placeholder="Provinsi">
   </div>
   <div class="col-lg-6">
    <label>Kabupaten / Kota</label>
    <input id="kabupatenIbu" type="text" class="form-control mb-10 form-reset" name="kabupatenIbu" placeholder="Kabupaten / Kota">
   </div>
  </div>
</div>

<div class="form-group">
 <div class="row">
  <div class="col-lg-6">
   <label>Kecamatan</label>
   <input id="kecamatanIbu"type="text" class="form-control mb-10 form-reset" name="kecamatanIbu" placeholder="Kecamatan">
  </div>
  <div class="col-lg-6">
   <label>Kelurahan</label>
   <input id="kelurahanIbu"type="text" class="form-control mb-10 form-reset" name="kelurahanIbu" placeholder="Kelurahan">
  </div>
 </div>
</div>

<div class="form-group">
<div class="row">
 <div class="col-lg-6">
  <label>Kewarganegaraan</label>
  <input id="kewarganegaraanIbu"type="text" class="form-control mb-10 form-reset" name="kewarganegaraanIbu" placeholder="Kewarganegaraan">
 </div>
 <div class="col-lg-6">
  <label>Kebangsaan</label>
  <input id="kebangsaanIbu"type="text" class="form-control mb-10 form-reset" name="kebangsaanIbu" placeholder="Kebangsaan">
 </div>
</div>
</div>

 <div class="form-group">
  <label>Tanggal Pencatatan Perkawinan</label>
  <div class="input-group">
  <div class="input-group-prepend">
   <span class="input-group-text"><i class="fa fa-calendar"></i></span>
  </div>
  <input id="tanggalKawin"name="tanggalKawin" type="text" class="form-control mb-10 form-reset" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
 </div>
 </div>


    <button class="btn btn-success" type="button" onclick="submitData()" name="button">Simpan</button>
   </form>
  </div>
 </div><!-- /.card-body -->
 </div>
 </section>
</div>
@endsection
<script type="text/javascript">
function submitData(){
 var dataByForm = $('#formLahir').serializeJSON();
 console.log(dataByForm);
 postAjax('{{ action('PelayananController@postLahir') }}',dataByForm,function(res){
   successAlert();
   window.location.replace(res.url)
   if(res.status == 'error'){
     errorAlert();
   }
   $('#formLahir')[0].reset();
 });
}

function postAjax(url,data,callback){
    $('#loading').fadeIn(200);
    $.ajax({
        url: url,
        data: data,
        method:'POST',
        dataType: 'json',
        success: function(res) {
         console.log(res);
         $('#loading').fadeOut(200);
         if(callback){
          callback(res)
         }
        }
        // error: function(xhr, status, error) {
        //   $('#loading').fadeOut(200);
        //   errorAlert(xhr.responseJSON.message);
        // }
    });
  }
</script>
