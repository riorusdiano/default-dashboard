@extends('layouts.main')
@section('title-module')
  Form Keterangan Nikah
@endsection

@section('content')
<div class="row">
 <section class="col-lg-12 connectedSortable ui-sortable">
  <!-- Custom tabs (Charts with tabs)-->
   <div class="card">
     <div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
       <h3 class="card-title p-3">Form Keterangan Nikah</h3>
     </div><!-- /.card-header -->
     <div class="card-body">
      <div class="col-lg-12">
      <form id="formNikah" method="post">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <input id="UserId" name="UserId" type="hidden" value="{{$session['UserId']}}">

       <div class="form-group">
        <div class="row">
         <div class="col-lg-3">
          <label>Kantor Kelurahan  :</label>
         </div>
         <div class="col-lg-3">
          <p>Aren Jaya</p>
         </div>
        </div>
       </div>

       <div class="form-group">
        <div class="row">
         <div class="col-lg-3">
          <label>Kecamatan  :</label>
         </div>
         <div class="col-lg-3">
          <p>Bekasi Timur</p>
         </div>
        </div>
       </div>

       <div class="form-group">
        <div class="row">
         <div class="col-lg-3">
          <label>Kota  :</label>
         </div>
         <div class="col-lg-3">
          <p>Bekasi</p>
         </div>
        </div>
       </div>

        <div class="form-group">
         <div class="row">
          <div class="col-lg-6">
           <label>Nama Lengkap</label>
           <input id="nama" type="text" class="form-control mb-10 form-reset" name="nama" placeholder="Nama Lengkap" value="<?=isset($detail) ? $detail['title'] : ''?>">
          </div>
          <div class="col-lg-6">
           <label>Jenis Kelamin</label>
           <select id="jenisKelamain" name="jenisKelamain" class="form-control" data-placeholder="Select a State" style="width: 100%;" tabindex="-1" aria-hidden="true">
             <option selected>Silahkan Pilih</option>
             <option>Laki-Laki</option>
             <option>Perempuan</option>
           </select>
          </div>
         </div>
        </div>

        <div class="form-group">
         <div class="row">
          <div class="col-lg-3">
           <label>Tempat</label>
           <input id="tempat" type="text" class="form-control mb-10 form-reset" name="tempat" placeholder="Tempat Lahir" value="<?=isset($detail) ? $detail['title'] : ''?>">
          </div>
          <div class="col-lg-9">
            <label>Tanggal Lahir</label>
            <div class="input-group">
            <div class="input-group-prepend">
             <span class="input-group-text"><i class="fa fa-calendar"></i></span>
            </div>
            <input class="form-control datepicker" id="tanggalLahir" name="tanggalLahir" placeholder="MM/DD/YYY" type="text"/>
           </div>
          </div>
         </div>
        </div>

        <div class="form-group">
         <div class="row">
          <div class="col-lg-4">
           <label>Agama</label>
           <input id="agama" type="text" class="form-control mb-10 form-reset" name="agama" placeholder="Agama" value="<?=isset($detail) ? $detail['title'] : ''?>">
          </div>

          <div class="col-lg-4">
           <label>Pekerjaan</label>
           <input id="pekerjaan" type="text" class="form-control mb-10 form-reset" name="pekerjaan" placeholder="Pekerjaan" value="<?=isset($detail) ? $detail['title'] : ''?>">
          </div>

          <div class="col-lg-4">
           <label>Tempat Tinggal</label>
           <input id="tempatTinggal" type="text" class="form-control mb-10 form-reset" name="tempatTinggal" placeholder="Tempat Tinggal" value="<?=isset($detail) ? $detail['title'] : ''?>">
         </div>
        </div>
       </div>

       <div class="form-group">
        <div class="row">
         <div class="col-lg-6">
          <label>Bin / Binti</label>
          <input id="bin" type="text" class="form-control mb-10 form-reset" name="bin" placeholder="Bin / Binti" value="<?=isset($detail) ? $detail['title'] : ''?>">
         </div>
         <div class="col-lg-6">
          <label>Status Perkawinan</label>
          <input id="status" type="text" class="form-control mb-10 form-reset" name="status" placeholder="Status Perkawinan" value="<?=isset($detail) ? $detail['title'] : ''?>">
         </div>
        </div>
       </div>

       <div class="form-group">
        <div class="row">
         <div class="col-lg-6">
          <label>1. Jika Pria Terangkan Jejaka, Duda atau Beristri Dan Istrinya Berapa</label>
          <input id="keteranganPria" type="text" class="form-control mb-10 form-reset" name="keteranganPria" value="">
         </div>
         <div class="col-lg-6">
          <label>2. Jika Wanita Terangkan Wanita atau Janda </label>
          <input id="keteranganWanita" type="text" class="form-control mb-10 form-reset"name="keteranganWanita" value="">
         </div>
        </div>
       </div>


        <div class="form-group">
         <div class="row">
          <div class="col-lg-4">
           <label>Nama Istri / Suami Terdahulu </label>
           <input id="sutriLalu" type="text" class="form-control mb-10 form-reset" name="sutriLalu" placeholder="Kecamatan" value="<?=isset($detail) ? $detail['title'] : ''?>">
          </div>
          <div class="col-lg-4">
           <label>Numpang Nikah</label>
           <input id="numpang" type="text" class="form-control mb-10 form-reset" name="numpang" placeholder="Kecamatan" value="<?=isset($detail) ? $detail['title'] : ''?>">
          </div>
          <div class="col-lg-4">
           <label>KUA Kecamatan</label>
           <input id="kua" type="text" class="form-control mb-10 form-reset" name="kua" placeholder="Kecamatan" value="<?=isset($detail) ? $detail['title'] : ''?>">
          </div>
         </div>
        </div>

       <button class="btn btn-success" type="button" onclick="submitData()" name="button">Simpan</button>
      </form>
     </div>
    </div><!-- /.card-body -->
    </div>
 </section>
</div>

@endsection
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript">
function submitData(){
 var dataByForm = $('#formNikah').serializeJSON();
 console.log(dataByForm);
 postAjax('{{ action('PelayananController@postNikah') }}',dataByForm,function(res){
   successAlert();
   window.location.replace(res.url)
   if(res.status == 'error'){
     errorAlert();
   }
   $('#formNikah')[0].reset();
 });
}

function postAjax(url,data,callback){
    $('#loading').fadeIn(200);
    $.ajax({
        url: url,
        data: data,
        method:'POST',
        dataType: 'json',
        success: function(res) {
         console.log(res);
         $('#loading').fadeOut(200);
         if(callback){
          callback(res)
         }
        }
        // error: function(xhr, status, error) {
        //   $('#loading').fadeOut(200);
        //   errorAlert(xhr.responseJSON.message);
        // }
    });
  }
</script>
