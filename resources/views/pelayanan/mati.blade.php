@extends('layouts.main')
@section('title-module')
  Form Keterangan Kematian
@endsection

@section('content')
<style>
 .font-color{
   color: red;
 }
</style>
<div class="row">
 <section class="col-lg-12 connectedSortable ui-sortable">
  <!-- Custom tabs (Charts with tabs)-->
   <div class="card">
     <div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
       <h3 class="card-title p-3">Form Keterangan Kematian</h3>
     </div><!-- /.card-header -->
     <div class="card-body">
      <div class="col-lg-12">
      <form id="formKematian" method="post">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <input id="UserId" name="UserId" type="hidden" value="{{$session['UserId']}}">
        <div class="form-group">
         <div class="row">
           <div class="col-lg-1">
            <label>No Surat</label>
           </div>
           <div class="col-lg-3">
            <p class="font-color">*)Diisi Petugas</p>
           </div>
           <div class="col-lg-12">
           <input type="text" class="form-control mb-10 form-reset" name="surat" placeholder="No Surat" value="<?=isset($detail) ? $detail['title'] : ''?>" disabled>
          </div>
         </div>
        </div>

        <div class="form-group">
         <div class="row">
          <div class="col-md-6">
           <label>Nama Kepala Keluarga</label>
           <input id="namaKepalaKeluarga" type="text" class="form-control mb-10 form-reset" name="namaKepalaKeluarga" placeholder="Nama Kepala Keluarga" value="<?=isset($detail) ? $detail['title'] : ''?>">
          </div>
         <div class="col-md-6">
          <label>Nomor Kepala Keluarga</label>
          <input id="nomorKepalaKeluarga"type="number" class="form-control mb-10 form-reset" name="nomorKepalaKeluarga" placeholder="Nomor Kepala Keluarga" value="<?=isset($detail) ? $detail['title'] : ''?>">
         </div>
        </div>
       </div>

       <div class="callout callout-info">
         <h1>Data Jenazah</h1>
       </div>

       <div class="form-group">
        <div class="row">
         <div class="col-lg-6">
          <label>NIK</label>
          <input id="nikJenazah"type="number" class="form-control mb-10 form-reset" name="nikJenazah" placeholder="Nomor Induk Kependudukan" value="<?=isset($detail) ? $detail['title'] : ''?>">
         </div>
         <div class="col-lg-6">
          <label>Nama Lengkap</label>
          <input id="namaJenazah"type="text" class="form-control mb-10 form-reset" name="namaJenazah" placeholder="Nama Lengkap" value="<?=isset($detail) ? $detail['title'] : ''?>">
         </div>
        </div>
       </div>

       <div class="form-group">
        <div class="row">
         <div class="col-lg-4">
          <label>Tempat</label>
          <input id="tempatLahirJenazah"type="text" class="form-control mb-10 form-reset" name="tempatLahirJenazah" placeholder="Tempat Lahir" value="<?=isset($detail) ? $detail['title'] : ''?>">
         </div>
         <div class="col-lg-2">
           <label>Tanggal Lahir</label>
           <div class="input-group">
           <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
           </div>
           <input id="tanggalLahirJenazah"name="tanggalLahirJenazah" type="text" class="form-control mb-10 form-reset" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
          </div>
         </div>
         <div class="col-lg-2">
           <label>Umur</label>
           <div class="input-group">
           <input id="umurJenazah" name="umurJenazah" type="number" class="form-control mb-10 form-reset" placeholder="Umur">
          </div>
         </div>
         <div class="col-lg-4">
          <label>Jenis Kelamin</label>
          <select id="kelaminJenazah" name="kelaminJenazah" class="form-control" data-placeholder="Select a State" style="width: 100%;" tabindex="-1" aria-hidden="true">
            <option selected>Silahkan Pilih</option>
            <option>Laki-Laki</option>
            <option>Perempuan</option>
          </select>
         </div>
        </div>
       </div>

       <div class="form-group">
        <div class="row">
         <div class="col-lg-6">
          <label>Agama</label>
          <input id="agamaJenazah" type="text" class="form-control mb-10 form-reset" name="agamaJenazah" placeholder="Agama" value="<?=isset($detail) ? $detail['title'] : ''?>">
         </div>
         <div class="col-lg-6">
          <label>Pekerjaan</label>
          <input id="pekerjaanJenazah" type="text" class="form-control mb-10 form-reset" name="pekerjaanJenazah" placeholder="Pekerjaan" value="<?=isset($detail) ? $detail['title'] : ''?>">
         </div>
        </div>
       </div>

        <div class="form-group">
         <label>Alamat</label>
         <br>
         <textarea id="alamatJenazah" name="alamatJenazah" rows="8" cols="80"></textarea>
        </div>

        <div class="form-group">
         <div class="row">
          <div class="col-lg-6">
           <label>Provinsi</label>
           <input id="provinsiJenazah"type="text" class="form-control mb-10 form-reset" name="provinsiJenazah" placeholder="Provinsi" value="">
          </div>
          <div class="col-lg-6">
           <label>Kabupaten / Kota</label>
           <input id="kabupatenJenazah" type="text" class="form-control mb-10 form-reset" name="kabupatenJenazah" placeholder="Kabupaten / Kota" value="">
          </div>
         </div>
       </div>

       <div class="form-group">
        <div class="row">
         <div class="col-lg-6">
          <label>Kecamatan</label>
          <input id="kecamatanJenazah" type="text" class="form-control mb-10 form-reset" name="kecamatanJenazah" placeholder="Kecamatan" value="">
         </div>
         <div class="col-lg-6">
          <label>Kelurahan</label>
          <input id="kelurahanJenazah" type="text" class="form-control mb-10 form-reset" name="kelurahanJenazah" placeholder="Kelurahan" value="">
         </div>
        </div>
      </div>

      <div class="form-group">
       <div class="row">
        <div class="col-lg-4">
         <label>Anak Ke-</label>
         <input id="anak_ke" type="number" class="form-control mb-10 form-reset" name="anak_ke" placeholder="" value="<?=isset($detail) ? $detail['title'] : ''?>">
        </div>
        <div class="col-lg-4">
          <label>Tanggal Kematian</label>
          <div class="input-group">
          <div class="input-group-prepend">
           <span class="input-group-text"><i class="fa fa-calendar"></i></span>
          </div>
          <input id="tanggalKematian" name="tanggalKematian" type="text" placeholder="12/08/1990"class="form-control mb-10 form-reset" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
         </div>
        </div>
        <div class="col-lg-4">
        <label>Pukul</label>
        <input id="pukul" type="number" class="form-control mb-10 form-reset" name="pukul" placeholder="12:00" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
      </div>
     </div>

     <div class="form-group">
      <label>Sebab Kematian</label>
       <div class="form-check">
        <div class="row">
         <div class="col-md-2">
          <input id="sebabKematian" name="sebabKematian" class="form-check-input" type="radio" value="Biasa">
          <label class="form-check-label">Sakit Biasa / Tua</label>
         </div>
         <div class="col-md-2">
          <input id="sebabKematian" name="sebabKematian" class="form-check-input" type="radio" value="Penyakit">
          <label class="form-check-label">Wabah Penyakit</label>
         </div>
         <div class="col-md-2">
          <input id="sebabKematian" name="sebabKematian" class="form-check-input" type="radio" value="Kecelakaan">
          <label class="form-check-label">Kecelakaan</label>
         </div>
         <div class="col-md-2">
          <input id="sebabKematian" name="sebabKematian" class="form-check-input" type="radio" value="Kriminalitas">
          <label class="form-check-label">Kriminalitas</label>
         </div>
         <div class="col-md-2">
          <input id="sebabKematian" name="sebabKematian" class="form-check-input" type="radio" value="Bunuh Diri">
          <label class="form-check-label">Bunuh Diri</label>
         </div>
         <div class="col-md-2">
          <input id="sebabKematian" name="sebabKematian" class="form-check-input" type="radio" value="Lainnya">
          <label class="form-check-label">Lainnya</label>
         </div>
        </div>
       </div>
     </div>

     <div class="form-group">
      <label>Tempat Kematian</label>
      <input id="tempatKematian" type="text" class="form-control mb-10 form-reset" name="tempatKematian" placeholder="Kecamatan" value="<?=isset($detail) ? $detail['title'] : ''?>">
     </div>

     <div class="form-group">
      <label>Yang Menerangkan</label>
       <div class="form-check">
        <div class="row">
         <div class="col-md-3">
          <input id="yangMenerangkan" name="yangMenerangkan" class="form-check-input" type="radio" value="Dokter">
          <label class="form-check-label">Dokter</label>
         </div>
         <div class="col-md-3">
          <input id="yangMenerangkan" name="yangMenerangkan" class="form-check-input" type="radio" value="Tenaga Kerja">
          <label class="form-check-label">Tenaga Kerja</label>
         </div>
         <div class="col-md-3">
          <input id="yangMenerangkan" name="yangMenerangkan" class="form-check-input" type="radio" value="Kepolisian">
          <label class="form-check-label">Kepolisian</label>
         </div>
         <div class="col-md-3">
          <input id="yangMenerangkan" name="yangMenerangkan" class="form-check-input" type="radio" value="Lainnya">
          <label class="form-check-label">Lainnya</label>
         </div>
        </div>
       </div>
     </div>

     <div class="callout callout-info">
       <h1>Data Ayah</h1>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>NIK</label>
        <input id="nikAyah" type="number" class="form-control mb-10 form-reset" name="nikAyah" placeholder="Nomor Induk Kependudukan" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
       <div class="col-lg-6">
        <label>Nama Lengkap</label>
        <input id="namaAyah"type="text" class="form-control mb-10 form-reset" name="namaAyah" placeholder="Nama Lengkap" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
      </div>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>Tempat</label>
        <input id="tempatLahirAyah" type="text" class="form-control mb-10 form-reset" name="tempatLahirAyah" placeholder="Tempat Lahir" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
       <div class="col-lg-3">
         <label>Tanggal Lahir</label>
         <div class="input-group">
         <div class="input-group-prepend">
          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
         </div>
         <input id="tanggalLahirAyah" name="tanggalLahirAyah" type="text" class="form-control mb-10 form-reset" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
        </div>
       </div>
       <div class="col-lg-3">
         <label>Umur</label>
         <div class="input-group">
         <input id="umurAyah" name="umurAyah" type="number" class="form-control mb-10 form-reset" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
        </div>
       </div>
      </div>
     </div>

     <div class="form-group">
      <label>Pekerjaan</label>
      <input id="pekerjaanAyah" type="text" class="form-control mb-10 form-reset" name="pekerjaanAyah" placeholder="Pekerjaan" value="<?=isset($detail) ? $detail['title'] : ''?>">
     </div>

     <div class="form-group">
      <label>Alamat</label>
      <br>
      <textarea id="alamatAyah" name="alamatAyah" rows="8" cols="80"></textarea>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>Provinsi</label>
        <input id="provinsiAyah" type="text" class="form-control mb-10 form-reset" name="provinsiAyah" placeholder="Provinsi" value="">
       </div>
       <div class="col-lg-6">
        <label>Kabupaten / Kota</label>
        <input id="kabupatenAyah" type="text" class="form-control mb-10 form-reset" name="kabupatenAyah" placeholder="Kabupaten / Kota" value="">
       </div>
      </div>
     </div>

     <div class="form-group">
     <div class="row">
      <div class="col-lg-6">
       <label>Kecamatan</label>
       <input id="kecamatanAyah" type="text" class="form-control mb-10 form-reset" name="kecamatanAyah" placeholder="Kecamatan" value="">
      </div>
      <div class="col-lg-6">
       <label>Kelurahan</label>
       <input id="kelurahanAyah" type="text" class="form-control mb-10 form-reset" name="kelurahanAyah" placeholder="Kelurahan" value="">
      </div>
     </div>
     </div>

     <div class="callout callout-info">
       <h1>Data Ibu</h1>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>NIK</label>
        <input id="nikIbu" type="number" class="form-control mb-10 form-reset" name="nikIbu" placeholder="Nomor Induk Kependudukan" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
       <div class="col-lg-6">
        <label>Nama Lengkap</label>
        <input id="namaIbu" type="text" class="form-control mb-10 form-reset" name="namaIbu" placeholder="Nama Lengkap" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
      </div>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>Tempat</label>
        <input id="tempatLahirIbu" type="text" class="form-control mb-10 form-reset" name="tempatLahirIbu" placeholder="Tempat Lahir" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
       <div class="col-lg-3">
         <label>Tanggal Lahir</label>
         <div class="input-group">
         <div class="input-group-prepend">
          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
         </div>
         <input id="tanggalLahirIbu" name="tanggalLahirIbu" type="text" class="form-control mb-10 form-reset" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
        </div>
       </div>
       <div class="col-lg-3">
         <label>Umur</label>
         <div class="input-group">
         <input id="umurIbu" name="umurIbu" type="number" class="form-control mb-10 form-reset" placeholder="Umur">
        </div>
       </div>
      </div>
     </div>

     <div class="form-group">
      <label>Pekerjaan</label>
      <input id="pekerjaanIbu" type="text" class="form-control mb-10 form-reset" name="pekerjaanIbu" placeholder="Pekerjaan" value="<?=isset($detail) ? $detail['title'] : ''?>">
     </div>

     <div class="form-group">
      <label>Alamat</label>
      <br>
      <textarea id="alamatIbu" name="alamatIbu" rows="8" cols="80"></textarea>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>Provinsi</label>
        <input id="provinsiIbu" type="text" class="form-control mb-10 form-reset" name="provinsiIbu" placeholder="Provinsi" value="">
       </div>
       <div class="col-lg-6">
        <label>Kabupaten / Kota</label>
        <input id="kabupatenIbu" type="text" class="form-control mb-10 form-reset" name="kabupatenIbu" placeholder="Kabupaten / Kota" value="">
       </div>
      </div>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>Kecamatan</label>
        <input id="kecamatanIbu" type="text" class="form-control mb-10 form-reset" name="kecamatanIbu" placeholder="Kecamatan" value="">
       </div>
       <div class="col-lg-6">
        <label>Kelurahan</label>
        <input id="kelurahanIbu" type="text" class="form-control mb-10 form-reset" name="kelurahanIbu" placeholder="Kelurahan" value="">
       </div>
      </div>
     </div>

     <div class="callout callout-info">
       <h1>Data Pelapor</h1>
     </div>
     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>NIK</label>
        <input id="nikPelapor" type="number" class="form-control mb-10 form-reset" name="nikPelapor" placeholder="Nomor Induk Kependudukan" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
       <div class="col-lg-6">
        <label>Nama Lengkap</label>
        <input id="namaPelapor" type="text" class="form-control mb-10 form-reset" name="namaPelapor" placeholder="Nama Lengkap" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
      </div>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>Tempat</label>
        <input id="tempatLahirPelapor" type="text" class="form-control mb-10 form-reset" name="tempatLahirPelapor" placeholder="Tempat Lahir" value="<?=isset($detail) ? $detail['title'] : ''?>">
       </div>
       <div class="col-lg-3">
         <label>Tanggal Lahir</label>
         <div class="input-group">
         <div class="input-group-prepend">
          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
         </div>
         <input id="tanggalLahirPelapor" name="tanggalLahirPelapor" type="text" class="form-control mb-10 form-reset" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
        </div>
       </div>
       <div class="col-lg-3">
         <label>Umur</label>
         <div class="input-group">
         <input id="umurPelapor" name="umurPelapor" type="number" class="form-control mb-10 form-reset" placeholder="Umur">
        </div>
       </div>
      </div>
     </div>

     <div class="form-group">
      <label>Pekerjaan</label>
      <input id="pekerjaanPelapor" type="text" class="form-control mb-10 form-reset" name="pekerjaanPelapor" placeholder="Pekerjaan" value="<?=isset($detail) ? $detail['title'] : ''?>">
     </div>

     <div class="form-group">
      <label>Alamat</label>
      <br>
      <textarea id="alamatPelapor" name="alamatPelapor" rows="8" cols="80"></textarea>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>Provinsi</label>
        <input id="provinsiPelapor" type="text" class="form-control mb-10 form-reset" name="provinsiPelapor" placeholder="Provinsi" value="">
       </div>
       <div class="col-lg-6">
        <label>Kabupaten / Kota</label>
        <input id="kabupatenPelapor" type="text" class="form-control mb-10 form-reset" name="kabupatenPelapor" placeholder="Kabupaten / Kota" value="">
       </div>
      </div>
     </div>

     <div class="form-group">
      <div class="row">
       <div class="col-lg-6">
        <label>Kecamatan</label>
        <input id="kecamatanPelapor"type="text" class="form-control mb-10 form-reset" name="kecamatanPelapor" placeholder="Kecamatan" value="">
       </div>
       <div class="col-lg-6">
        <label>Kelurahan</label>
        <input id="kelurahanPelapor" type="text" class="form-control mb-10 form-reset" name="kelurahanPelapor" placeholder="Kelurahan" value="">
       </div>
      </div>
     </div>
     <button class="btn btn-success" type="button" onclick="submitData()" name="button">Simpan</button>
     </form>
    </div>
   </div><!-- /.card-body -->
  </div>
 </section>
</div>
@endsection
<script type="text/javascript">
function submitData(){
 var dataByForm = $('#formKematian').serializeJSON();
 console.log(dataByForm);
 postAjax('{{ action('PelayananController@postKematian') }}',dataByForm,function(res){
   successAlert();
   window.location.replace(res.url)
   if(res.status == 'error'){
     errorAlert();
   }
   $('#formKematian')[0].reset();
 });
}

function postAjax(url,data,callback){
    $('#loading').fadeIn(200);
    $.ajax({
        url: url,
        data: data,
        method:'POST',
        dataType: 'json',
        success: function(res) {
         console.log(res);
         $('#loading').fadeOut(200);
         if(callback){
          callback(res)
         }
        }
        // error: function(xhr, status, error) {
        //   $('#loading').fadeOut(200);
        //   errorAlert(xhr.responseJSON.message);
        // }
    });
  }
</script>
