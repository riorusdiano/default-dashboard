@extends('layouts.main')
@section('title-module')
  List Permohonon KK
@endsection

@section('content')
<div class="row">
 <div class="col-md-12">
  <a href="{!!url('/kk') !!}" class="btn btn-block btn-info btn-lg col-md-1">
   <span><i class="fa fa-plus-square"></i></span> Buat Baru
 </a>
 </div>
</div>
<br/>
<div class="row">
 <div class="col-md-12">

 <div class="card">
   <!-- /.card-header -->
   <div class="card-body p-0">
     <table class="table table-condensed">
       <tbody><tr>
         <th style="width: 10px">#</th>
         <th>Nama Lengkap</th>
         <th>Jenis Permohonan</th>
         <th>Alamat</th>
         <th>Created At</th>
         <th style="width: 40px">Actions</th>
       </tr>
       <?php
       $i = 1;
       ?>
       @foreach($result as $key => $row)
       <tr>
         <td>{{$i++}}</td>
         <td>{{$row->Nama}}</td>
         <td>{{$row->Alasan}}</td>
         <td>{{$row->Alamat}}</td>
         <td><?= date("d-m-Y", strtotime($row->created_at))?></td>
         <td>
          <div class="btn-group">
             <button type="button" id="formId" name="formId" onclick="printForm({{$row->Id}})" class="btn btn-info"><i class="fa fa-print"></i></button>
             <button type="button" onclick="deleteData({{$row->Id}})" data-token="{{ csrf_token() }}" class="btn btn-danger"><i class="fa fa-trash"></i></button>
           </div>
         </td>
      </tr>
      @endforeach

     </tbody>
    </table>
   </div>
   <!-- /.card-body -->
 </div>
 <!-- /.card -->
</div>
</div>
@endsection
<script type="text/javascript">
function deleteData(id){
 swal('Are you sure remove this product?', {
      buttons: {
        cancel: 'Cancel',
        catch: {
          text: 'Remove product',
          value: 'remove',
        },
      },
    }).then((value) => {
          switch (value) {
            case 'remove':
            let data = {
             id: id,
             type: 'FormKK'
            };
            postAjax('{{ action('PelayananController@deleteData')}}', data, function(res){
             successAlert();
             window.location.replace(res.url)
            });
              break;
            default:
              swal('Cancel print ticket');
              break;
          }
      });
}

function postAjax(url,data,callback){
 console.log(data);
    $('#loading').fadeIn(200);
    $.ajax({
        url: url,
        data: data,
        method:'POST',
        beforeSend: function(xhr) {
                      xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                    },
        dataType: 'json',
        success: function(res) {
         console.log(res);
         $('#loading').fadeOut(200);
         if(callback){
          callback(res)
         }
        }
        // error: function(xhr, status, error) {
        //   $('#loading').fadeOut(200);
        //   errorAlert(xhr.responseJSON.message);
        // }
    });
  }
function printForm(id){
 swal('Are you sure print this ticket?', {
      buttons: {
        cancel: 'Cancel',
        catch: {
          text: 'Print ticket',
          value: 'print',
        },
      },
    }).then((value) => {
          switch (value) {
            case 'print':
            let data = {id: id};
            printAjax('{{ action('ItemController@pdfkk')}}', data, function(res){
             // console.log(res);
            });
              break;
            default:
              swal('Cancel print ticket');
              break;
          }
      });
}

function printAjax(url, data, callback){
 $('#loading').fadeIn(200);
 $.ajax({
     url: url,
     data: data,
     method:'GET',
     contentType: false,
     xhrFields: {
               responseType: 'blob'
           },
     success: function(response, status, xhr) {
      var filename = "";
        var disposition = xhr.getResponseHeader('Content-Disposition');
         if (disposition) {
            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            var matches = filenameRegex.exec(disposition);
            if (matches !== null && matches[1]) filename = matches[1].replace(/['"]/g, '');
        }
        var linkelem = document.createElement('a');
        try {
         var blob = new Blob([response], { type: 'application/octet-stream' });
         if (typeof window.navigator.msSaveBlob !== 'undefined') {
             //   IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
             window.navigator.msSaveBlob(blob, filename);
         } else {
             var URL = window.URL || window.webkitURL;
             var downloadUrl = URL.createObjectURL(blob);

             if (filename) {
                 // use HTML5 a[download] attribute to specify filename
                 var a = document.createElement("a");

                 // safari doesn't support this yet
                 if (typeof a.download === 'undefined') {
                     window.location = downloadUrl;
                 } else {
                     a.href = downloadUrl;
                     a.download = filename;
                     document.body.appendChild(a);
                     a.target = "_blank";
                     a.click();
                 }
             } else {
                 window.location = downloadUrl;
             }
         }
        } catch (ex) {
       }
     }
 });
}
</script>
