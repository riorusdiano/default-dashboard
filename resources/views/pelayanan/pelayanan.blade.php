@extends('layouts.main')
@section('title-module')
  Pelayanan
@endsection
@section('content')
<div class="row">

  <div class="col-md-3 col-sm-6 col-12">
    @if($session['Role'] == 'user')
    <a href="myListKtp">
    @elseif($session['Role'] == 'admin')
    <a href="listKtp">
    @endif
    <div class="info-box">
      <span class="info-box-icon bg-info"><i class="fa fa-files-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text"><p>Form Permohonan KTP</p></span>
      </div>
    </div>
   </a>
  </div>
  <!-- /.col -->

  <div class="col-md-3 col-sm-6 col-12">
   @if($session['Role'] == 'user')
   <a href="myListKK">
   @elseif($session['Role'] == 'admin')
   <a href="listKK">
   @endif
    <div class="info-box">
      <span class="info-box-icon bg-success"><i class="fa fa-files-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Form Permohonan KK</span>
      </div>
      <!-- /.info-box-content -->
    </div>
   </a>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-12">
   @if($session['Role'] == 'user')
   <a href="myListNikah">
   @elseif($session['Role'] == 'admin')
   <a href="listNikah">
   @endif
    <div class="info-box">
      <span class="info-box-icon bg-warning"><i class="fa fa-files-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Form Keterangan Nikah</span>
      </div>
      <!-- /.info-box-content -->
    </div>
   </a>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-12">
   @if($session['Role'] == 'user')
   <a href="myListLahir">
   @elseif($session['Role'] == 'admin')
   <a href="listKelahiran">
   @endif
    <div class="info-box">
      <span class="info-box-icon bg-primary-gradient"><i class="fa fa-files-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Form Keterangan Kelahiran</span>
      </div>
    </div>
   </a>
  </div>

  <div class="col-md-3 col-sm-6 col-12">
   @if($session['Role'] == 'user')
   <a href="myListKematian">
   @elseif($session['Role'] == 'admin')
   <a href="listKematian">
   @endif
    <div class="info-box">
      <span class="info-box-icon bg-danger"><i class="fa fa-files-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Form Keterangan Kematian</span>
      </div>
    </div>
   </a>
  </div>
 </div>
@endsection
