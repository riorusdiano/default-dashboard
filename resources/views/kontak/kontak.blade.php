@extends('layouts.main')
@section('title-module')
  Kontak
@endsection

@section('content')
<div class="row">
 <section class="col-lg-3 connectedSortable ui-sortable">
  <!-- Custom tabs (Charts with tabs)-->
   <div class="card">
     <div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
       <h3 class="card-title p-3">
        <i class="nav-icon fa fa-phone"></i>
         Layanan Informasi
       </h3>
     </div><!-- /.card-header -->
     <div class="card-body">
      <p>Lokasi: <b>Kelurahan Aren Jaya Bekasi Timur</b></p>
      <p>Alamat: <b>Jalan Nusantara Raya No. 1</b></p>
      <p>No Telp: <b>021-88349483</b></p>
     </div><!-- /.card-body -->
   </div>
   <!-- /.card -->
   <!--/.direct-chat -->
 </section>
 <section class="col-lg-9 connectedSortable ui-sortable">
  <!-- Custom tabs (Charts with tabs)-->
   <div class="card">
     <div class="card-header d-flex p-0 ui-sortable-handle" style="cursor: move;">
       <h3 class="card-title p-3">
        <i class="nav-icon fa fa-map"></i>
         Map
       </h3>
     </div><!-- /.card-header -->
     <div class="card-body">
      <img src="dist/img/lokasi.png" style="width:100%;">
     </div><!-- /.card-body -->
   </div>
   <!-- /.card -->
 </section>
</div>
@endsection
