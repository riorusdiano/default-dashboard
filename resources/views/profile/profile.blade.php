@extends('layouts.main')
@section('title-module')
  Profile
@endsection

@section('content')
<div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Profil foto</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4" class=""></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5" class=""></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="6" class=""></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img style="height:600px;" class="d-block w-100" src="dist/img/profile.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                      <img style="height:600px;" class="d-block w-100" src="dist/img/profile1.jpg" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                      <img style="height:600px;" class="d-block w-100" src="dist/img/profile3.jpg" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                      <img style="height:600px;" class="d-block w-100" src="dist/img/profile5.jpg" alt="Fourth slide">
                    </div>
                    <div class="carousel-item">
                      <img style="height:600px;" class="d-block w-100" src="dist/img/profile6.jpg" alt="Fifth slide">
                    </div>
                    <div class="carousel-item">
                      <img style="height:600px;" class="d-block w-100" src="dist/img/profile8.jpg" alt="Sixth slide">
                    </div>
                    <div class="carousel-item">
                      <img style="height:600px;" class="d-block w-100" src="dist/img/profile9.jpg" alt="Seven slide">
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
@endsection
