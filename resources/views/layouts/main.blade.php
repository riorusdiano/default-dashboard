<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<!-- begin::Head -->
<head>
  <meta charset="utf-8" />
  <title>Riset Dashboard</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="description" content="Latest updates and statistic charts">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!--begin::Web font -->
@section('head')
    @include('general.head')
@show
</head>

@include('general.header')
@include('general.sidebar')
  <div class="content-wrapper">
   <div class="content-header">
    <div class="container-fluid">
     <div class="row mb-2">
       <div class="col-sm-6">
         <h1 class="m-0 text-dark">@yield('title-module')</h1>
       </div><!-- /.col -->
       <div class="col-sm-6">
         <ol class="breadcrumb float-sm-right">
           <li class="breadcrumb-item"><a href="home">Home</a></li>
           <li class="breadcrumb-item active">@yield('title-module')</li>
         </ol>
       </div>
     </div>
    </div>
   </div>

  <section class="content">
   <div class="container-fluid">
    @yield('content')
   </div>
  </section>
 </div>
@include('general.footer')
  </div>
 </body>
</html>
